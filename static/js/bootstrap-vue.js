(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('tether')) :
	typeof define === 'function' && define.amd ? define(['tether'], factory) :
	(global.bootstrapVue = factory(global.Tether));
}(this, (function (Tether) { 'use strict';

Tether = 'default' in Tether ? Tether['default'] : Tether;

var alert = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return (_vm.localShow)?_c('div',{class:_vm.classObject,attrs:{"role":"alert"}},[(_vm.dismissible)?_c('button',{staticClass:"close",attrs:{"type":"button","data-dismiss":"alert","aria-label":"Close"},on:{"click":function($event){$event.stopPropagation();$event.preventDefault();_vm.dismiss($event);}}},[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("×")])]):_vm._e(),_vm._t("default")],2):_vm._e()},staticRenderFns: [],
    data: function data() {
        return {
            countDownTimerId: null,
            dismissed: false
        };
    },
    created: function created() {
        if (this.state) {
            console.warn('<b-alrt> state property is deprecated, please use variant instead.');
        }
    },
    computed: {
        classObject: function classObject() {
            return ['alert', this.alertVariant, this.dismissible ? 'alert-dismissible' : ''];
        },
        alertVariant: function alertVariant() {
            var variant = this.state || this.variant || 'info';
            return ("alert-" + variant);
        },
        localShow: function localShow() {
            return !this.dismissed && (this.countDownTimerId || this.show);
        }
    },
    props: {
        variant: {
            type: String,
            default: 'info'
        },
        state: {
            type: String,
            default: null
        },
        dismissible: {
            type: Boolean,
            default: false
        },
        show: {
            type: [Boolean, Number],
            default: false
        }
    },
    watch: {
        show: function show() {
            this.showChanged();
        }
    },
    mounted: function mounted() {
        this.showChanged();
    },
    methods: {
        dismiss: function dismiss() {
            this.dismissed = true;
            this.$emit('dismissed');
            this.clearCounter();
        },
        clearCounter: function clearCounter() {
            if (this.countDownTimerId) {
                clearInterval(this.countDownTimerId);
            }
        },
        showChanged: function showChanged() {
            var this$1 = this;

            // Reset dismiss status
            this.dismissed = false;

            // No timer for boolean values
            if (this.show === true || this.show === false || this.show === null || this.show === 0) {
                return;
            }

            var dismissCountDown = this.show;
            this.$emit('dismiss-count-down', dismissCountDown);

            // Start counter
            this.clearCounter();
            this.countDownTimerId = setInterval(function () {
                if (dismissCountDown < 2) {
                    return this$1.dismiss();
                }
                dismissCountDown--;
                this$1.$emit('dismiss-count-down', dismissCountDown);
            }, 1000);
        }
    }
};

var breadcrumb = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('ol',{staticClass:"breadcrumb"},[_vm._l((_vm.items),function(item){return (_vm.items)?_c('li',{class:['breadcrumb-item', item.active ? 'active' : null]},[(item.active)?_c('span',{domProps:{"innerHTML":_vm._s(item.text)}}):_c('b-link',{attrs:{"to":item.to||item.href||item.link},domProps:{"innerHTML":_vm._s(item.text)}})],1):_vm._e()}),_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        componentType: function componentType() {
            return this.to ? 'router-link' : 'a';
        }
    },
    props: {
        items: {
            type: Array,
            default: function () { return []; },
            required: true
        }
    },
    methods: {
        onclick: function onclick(item) {
            this.$emit('click', item);
            if (this.$router && this.to) {
                this.$router.push(this.to);
            }
        }
    }
};

var buttonCheckbox = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"btn-group",attrs:{"data-toggle":"buttons"}},_vm._l((_vm.list),function(item,index){return _c('label',{class:['btn', _vm.btnVariant, _vm.btnSize, _vm.checked(index) ? 'active' : '']},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(item.checked),expression:"item.checked"}],attrs:{"type":"checkbox","autocomplete":"off","disabled":item.disabled},domProps:{"value":item.value,"checked":Array.isArray(item.checked)?_vm._i(item.checked,item.value)>-1:(item.checked),"innerHTML":_vm._s(item.text)},on:{"__c":function($event){var $$a=item.checked,$$el=$event.target,$$c=$$el.checked?(true):(false);if(Array.isArray($$a)){var $$v=item.value,$$i=_vm._i($$a,$$v);if($$c){$$i<0&&(item.checked=$$a.concat($$v));}else{$$i>-1&&(item.checked=$$a.slice(0,$$i).concat($$a.slice($$i+1)));}}else{item.checked=$$c;}}}})])}))},staticRenderFns: [],
    replace: true,
    computed: {
        btnVariant: function btnVariant() {
            return !this.variant || this.variant === "default" ? "btn-secondary" : ("btn-" + (this.variant));
        },
        btnSize: function btnSize() {
            return !this.size || this.size === "default" ? "" : ("btn-" + (this.size));
        }
    },
    props: {
        list: {
            type: Array,
            default: [],
            required: true
        },
        model: {
            type: Array,
            default: []
        },
        size: {
            type: String,
            default: 'md'
        },
        variant: {
            type: String,
            default: 'default'
        },
        returnObject: {
            type: Boolean,
            default: false
        }
    },
    methods: {
        checked: function checked(index) {
            var this$1 = this;

            if (!this.list) {
                return false;
            }
            var result = false;
            if (this.returnObject) {
                for (var i = 0; i < this.model.length; i++) {
                    if (this$1.model[i].value === this$1.list[index].value) {
                        result = true;
                    }
                }
            } else {
                result = this.model.indexOf(this.list[index].value) !== -1;
            }
            return result;
        }
    },
    watch: {
        list: {

        }
    },
    mounted: function mounted() {
        var this$1 = this;

        // Handle initial selection
        this.list.forEach(function (item) {
            if (this$1.returnObject) {
                this$1.model.forEach(function (modelItem) {
                    if (modelItem.value === item.value
                    ) {
                        item.checked = true;
                    }
                });
            } else if (this$1.model.indexOf(item.value) !== -1) {
                item.checked = true;
            }
        });
    }
};

var buttonGroup = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:_vm.classObject,attrs:{"role":"group"}},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return [
                'btn-' + (this.toolbar ? 'toolbar' : 'group'),
                this.vertical ? 'btn-group-vertical' : '',
                this.size ? ('btn-group-' + this.size) : ''
            ];
        }
    },
    props: {
        vertical: {
            type: Boolean,
            default: false
        },
        toolbar: {
            type: Boolean,
            default: false
        },
        size: {
            type: String,
            default: null
        }
    }
};

var buttonRadio = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"btn-group",attrs:{"data-toggle":"buttons"}},_vm._l((_vm.list),function(item){return _c('label',{class:['btn', _vm.btnVariant, _vm.btnSize, {disabled:item.disabled, active:_vm.selection == item.value}]},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.selection),expression:"selection"}],attrs:{"type":"radio","name":"options","autocomplete":"off","disabled":item.disabled},domProps:{"value":item.value,"checked":_vm._q(_vm.selection,item.value),"innerHTML":_vm._s(item.text)},on:{"__c":function($event){_vm.selection=item.value;}}})])}))},staticRenderFns: [],
    data: function data() {
        return {
            selection: '',
            model: {}
        };
    },
    computed: {
        btnVariant: function btnVariant() {
            return !this.variant || this.variant === "default" ? "btn-secondary" : ("btn-" + (this.variant));
        },
        btnSize: function btnSize() {
            return !this.size || this.size === "default" ? "" : ("btn-" + (this.size));
        }
    },
    props: {
        list: {
            type: Array,
            default: [],
            required: true
        },
        size: {
            type: String,
            default: 'md'
        },
        variant: {
            type: String,
            default: 'default'
        },
        returnObject: {
            type: Boolean,
            default: false
        }
    },
    watch: {
        selection: {
            handler: function handler() {
                var this$1 = this;

                // Set the model based on selection
                if (this.returnObject) {
                    this.list.forEach(function (item) {
                        if (item.value === this$1.selection) {
                            this$1.model = item;
                        }
                    });
                } else {
                    this.model = this.selection;
                }
                // Emit an event
                this.$root.$emit('changed::button-radio', this.model);
            },
            deep: true
        }
    },
    mounted: function mounted() {
        // Handle initial selection
        this.selection = this.model.value;
    }
};

var bButton = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.componentType,{tag:"button",class:_vm.classObject,attrs:{"to":_vm.to,"href":_vm.href},on:{"click":_vm.onclick}},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return [
                'btn',
                this.btnVariant,
                this.btnSize,
                this.btnBlock,
                this.btnDisabled,
                this.inactive ? 'btn-inactive' : ''
            ];
        },
        componentType: function componentType() {
            return (this.href || this.to) ? 'b-link' : 'button';
        },
        btnBlock: function btnBlock() {
            return this.block ? "btn-block" : '';
        },
        btnVariant: function btnVariant() {
            return this.variant ? ("btn-" + (this.variant)) : "btn-secondary";
        },
        btnSize: function btnSize() {
            return this.size ? ("btn-" + (this.size)) : '';
        },
        btnDisabled: function btnDisabled() {
            return this.disabled ? 'disabled' : '';
        }
    },
    props: {
        block: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        inactive: {
            type: Boolean,
            default: false
        },
        role: {
            type: String,
            default: ''
        },
        size: {
            type: String,
            default: 'md'
        },
        variant: {
            type: String,
            default: null
        },
        to: {
            type: [String, Object]
        },
        href: {
            type: String
        }
    },
    methods: {
        onclick: function onclick(e) {
            this.$emit('click', e);
        }
    }
};

var card = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['card',_vm.cardVariant,_vm.cardAlign,_vm.cardInverse]},[_vm._t("img",[(_vm.img)?_c('img',{staticClass:"card-img",attrs:{"src":_vm.img,"alt":_vm.imgAlt}}):_vm._e()]),(_vm.header || _vm.showHeader)?_c(_vm.headerTag,{tag:"component",staticClass:"card-header"},[_vm._t("header",[_c('div',{domProps:{"innerHTML":_vm._s(_vm.header)}})])],2):_vm._e(),(_vm.noBlock)?[_vm._t("default")]:_c('div',{class:_vm.blockClass},[(_vm.title)?_c('h4',{staticClass:"card-title",domProps:{"innerHTML":_vm._s(_vm.title)}}):_vm._e(),(_vm.subTitle)?_c('h6',{staticClass:"card-subtitle mb-2 text-muted",domProps:{"innerHTML":_vm._s(_vm.subTitle)}}):_vm._e(),_vm._t("default")],2),(_vm.footer || _vm.showFooter)?_c(_vm.footerTag,{tag:"component",staticClass:"card-footer"},[_vm._t("footer",[_c('div',{domProps:{"innerHTML":_vm._s(_vm.footer)}})])],2):_vm._e()],2)},staticRenderFns: [],
    computed: {
        blockClass: function blockClass() {
            return [
                'card-block',
                this.overlay ? 'card-img-overlay' : null
            ];
        },
        cardVariant: function cardVariant() {
            return this.variant ? ("card-" + (this.variant)) : null;
        },
        cardInverse: function cardInverse() {
            if (this.overlay || this.inverse) {
                return 'card-inverse';
            }
            // Auto inverse colored cards
            if (this.inverse === null && this.variant && this.variant.length > 0 &&
                this.variant.indexOf('outline') === -1) {
                return 'card-inverse';
            }
        },
        cardAlign: function cardAlign() {
            return ("text-" + (this.align));
        }
    },
    props: {
        align: {
            type: String,
            default: 'left'
        },
        inverse: {
            type: Boolean,
            // It should remain null for auto inverse
            default: null
        },
        variant: {
            type: String,
            default: null
        },

        // Header
        header: {
            type: String,
            default: null
        },
        showHeader: {
            type: Boolean,
            default: false
        },
        headerTag: {
            type: String,
            default: 'div'
        },

        // Footer
        footer: {
            type: String,
            default: null
        },
        showFooter: {
            type: Boolean,
            default: false
        },
        footerTag: {
            type: String,
            default: 'div'
        },

        // Main block
        title: {
            type: String,
            default: null
        },
        subTitle: {
            type: String,
            default: null
        },
        noBlock: {
            type: Boolean,
            default: false
        },

        // Image
        img: {
            type: String,
            default: null
        },
        imgAlt: {
            type: String,
            default: null
        },
        overlay: {
            type: Boolean,
            default: false
        }
    }
};

var cardGroup = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['card-' + _vm.type]},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        type: function type() {
            if (this.deck) {
                return 'deck';
            }

            if (this.columns) {
                return 'columns';
            }

            return 'group';
        }
    },
    props: {
        deck: {
            type: Boolean,
            default: false
        },
        columns: {
            type: Boolean,
            default: false
        }
    }
};

/* eslint-disable no-var, no-undef, guard-for-in, object-shorthand */

// const inBrowser = typeof window !== 'undefined';

// pulled from http://stackoverflow.com/questions/1349404/generate-a-string-of-5-random-characters-in-javascript
function uniqueId() {
    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

// Check if browser support css3 transitions
function csstransitions() {
    if (typeof (document) === 'undefined') {
        return false;
    }
    var style = document.documentElement.style;
    return (
        style.webkitTransition !== undefined ||
        style.MozTransition !== undefined ||
        style.OTransition !== undefined ||
        style.MsTransition !== undefined ||
        style.transition !== undefined
    );
}

// For browsers that do not support transitions like IE9 just change immediately

/**
 * Carousel Notes
 * - Ie9 does not support transitions and might require javascript fallbacks. B4 deliberately dropped support for this.
 * - It is not accessible.
 *
 * How it works:
 * - active element applies the transition to the slide but not triggers it
 * - we need to use 'right' and 'left' classes to trigger animation
 * - 'next' and 'prev' class makes the incoming slide positioned absolute, so it can follow outgoing slide
 *
 * To slide right to left we have to:
 * - add class 'active', 'next', and right to the next slide
 * - add class 'left' on the current slide same time as remove the 'right' class on the incoming one
 * - remove all classes and only leave 'active' on the incoming slide
 *
 */

// This is directly linked to the bootstrap animation timing in _carousel.scss
// For browsers that do not support transitions like IE9 just change slide immediately
var TRANSITION_DURATION$$1 = csstransitions() ? 600 : 0;

// When next is set, we want to move from right to left
// When previous is set, we want to move from left to right
var DIRECTION = {
    rtl: {
        outgoing: 'left',
        incoming: 'right',
        overlay: 'next'
    },
    ltr: {
        outgoing: 'right',
        incoming: 'left',
        overlay: 'prev'
    }
};

var carousel = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"carousel slide",attrs:{"data-ride":"carousel"},on:{"mouseenter":function($event){_vm.pause();},"mouseleave":function($event){_vm.start();}}},[_c('ol',{directives:[{name:"show",rawName:"v-show",value:(_vm.indicators),expression:"indicators"}],staticClass:"carousel-indicators"},_vm._l((_vm.slides),function(item,indicatorIndex){return _c('li',{class:{active:indicatorIndex === _vm.index},on:{"click":function($event){_vm.changeSlide(indicatorIndex);}}})})),_c('div',{staticClass:"carousel-inner",attrs:{"role":"listbox"}},[_vm._t("default")],2),_c('a',{directives:[{name:"show",rawName:"v-show",value:(_vm.controls),expression:"controls"}],staticClass:"carousel-control-prev",attrs:{"href":"#","role":"button","data-slide":"prev"},on:{"click":function($event){$event.stopPropagation();$event.preventDefault();_vm.prev($event);}}},[_c('span',{staticClass:"carousel-control-prev-icon",attrs:{"aria-hidden":"true"}}),_vm._v(" "),_c('span',{staticClass:"sr-only"},[_vm._v("Previous")])]),_c('a',{directives:[{name:"show",rawName:"v-show",value:(_vm.controls),expression:"controls"}],staticClass:"carousel-control-next",attrs:{"href":"#","role":"button","data-slide":"next"},on:{"click":function($event){$event.stopPropagation();$event.preventDefault();_vm.next($event);}}},[_c('span',{staticClass:"carousel-control-next-icon",attrs:{"aria-hidden":"true"}}),_vm._v(" "),_c('span',{staticClass:"sr-only"},[_vm._v("Next")])])])},staticRenderFns: [],
    replace: true,
    computed: {},
    data: function data() {
        return {
            index: 0,
            slidesCount: 0,
            animating: false,
            slides: [],
            direction: DIRECTION.rtl
        };
    },
    props: {
        interval: {
            type: Number,
            default: 5000
        },
        indicators: {
            type: Boolean,
            default: true
        },
        controls: {
            type: Boolean,
            default: true
        }
    },
    methods: {
        // Previous slide
        prev: function prev() {
            if (this.animating) {
                return;
            }
            this.index--;
            if (this.index < 0) {
                this.index = this.slidesCount;
            }
        },
        // Next slide
        next: function next() {
            if (this.animating) {
                return;
            }
            this.index++;
            if (this.index > this.slidesCount) {
                this.index = 0;
            }
        },
        // On slide change
        changeSlide: function changeSlide(index) {
            this.index = index;
        },
        // Pause auto rotation
        pause: function pause() {
            if (this.interval === 0 || typeof this.interval === 'undefined') {
                return;
            }
            clearInterval(this._intervalId);
        },
        // Start auto rotate slides
        start: function start() {
            var this$1 = this;

            if (this.interval === 0 || typeof this.interval === 'undefined') {
                return;
            }
            this._intervalId = setInterval(function () {
                this$1.next();
            }, this.interval);
        }
    },
    mounted: function mounted() {
        // Get all slides
        this._items = this.$el.querySelectorAll('.carousel-item');
        this.slidesCount = this._items.length - 1;
        this.slides = Array.apply(null, {length: this._items.length}).map(Number.call, Number);

        // Set first slide as active
        this._items[0].classList.add('active');

        // Auto rotate slides
        this.start();
    },
    watch: {
        index: function index(val, oldVal) {
            var this$1 = this;

            this.animating = true;
            this.direction = DIRECTION.rtl;

            // When previous is pressed we want to move from left to right
            if (val < oldVal) {
                this.direction = DIRECTION.ltr;
            }

            // Lets animate
            // prepare next slide to animate (position it on the opposite side of the direction as a starting point)
            this._items[val].classList.add(this.direction.incoming, this.direction.overlay);
            // Reflow
            // this._items[val].offsetWidth;
            // add class active
            this._items[val].classList.add('active');
            // Trigger animation on outgoing and incoming slide
            this._items[oldVal].classList.add(this.direction.outgoing);
            this._items[val].classList.remove(this.direction.incoming);
            // Wait for animation to finish and cleanup classes
            this._carouselAnimation = setTimeout(function () {
                this$1._items[oldVal].classList.remove(this$1.direction.outgoing, 'active');
                this$1._items[val].classList.remove(this$1.direction.overlay);
                this$1.animating = false;
                // Trigger an event
                this$1.$root.$emit('slid::carousel', val);
            }, TRANSITION_DURATION$$1);
        }
    },
    destroyed: function destroyed() {
        clearTimeout(this._carouselAnimation);
        clearInterval(this._intervalId);
    }
};

var carouselSlide = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"carousel-item"},[_vm._t("default")],2)},staticRenderFns: [],
    replace: true
};

var collapse = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"name":"collapse"}},[_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.show),expression:"show"}],class:_vm.classObject},[_vm._t("default")],2)])},staticRenderFns: [],

    data: function data() {
        return {
            show: false
        };
    },

    computed: {
        classObject: function classObject() {
            return {
                'navbar-collapse': this.isNav,
                show: this.show
            };
        }
    },

    props: {
        isNav: {
            type: Boolean,
            default: false
        },
        id: {
            type: String,
            required: true
        }
    },

    methods: {
        toggle: function toggle() {
            this.show = !this.show;
        }
    },

    created: function created() {
        var this$1 = this;

        this.$root.$on('collapse::toggle', function (target) {
            if (target !== this$1.id) {
                return;
            }
            this$1.toggle();
        });
    }
};

var clickOut = {
    mounted: function mounted() {
        if (typeof document !== 'undefined') {
            document.documentElement.addEventListener('click', this._clickOutListener);
        }
    },
    destroyed: function destroyed() {
        if (typeof document !== 'undefined') {
            document.removeEventListener('click', this._clickOutListener);
        }
    },
    methods: {
        _clickOutListener: function _clickOutListener(e) {
            if (!this.$el.contains(e.target)) {
                if (this.clickOutListener) {
                    this.clickOutListener();
                }
            }
        }
    }
};

var dropdown = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['dropdown','btn-group',_vm.visible?'show':'',_vm.dropup?'dropup':'']},[_c('b-button',{class:[_vm.split?'':'dropdown-toggle'],attrs:{"aria-haspopup":"true","aria-expanded":_vm.visible,"variant":_vm.variant,"size":_vm.size,"disabled":_vm.disabled},on:{"click":_vm.click}},[_vm._t("text",[_vm._v(_vm._s(_vm.text))])],2),(_vm.split)?_c('b-button',{staticClass:"dropdown-toggle dropdown-toggle-split",attrs:{"variant":_vm.variant,"size":_vm.size,"disabled":_vm.disabled},on:{"click":_vm.toggle}},[_c('span',{staticClass:"sr-only"},[_vm._v("Toggle Dropdown")])]):_vm._e(),_c('div',{class:['dropdown-menu',_vm.right?'dropdown-menu-right':'']},[_vm._t("default")],2)],1)},staticRenderFns: [],
    mixins: [
        clickOut
    ],
    components: {
        bButton: bButton
    },
    data: function data() {
        return {
            visible: false
        };
    },
    props: {
        split: {
            type: Boolean,
            default: false
        },
        text: {
            type: String,
            default: ''
        },
        size: {
            type: String,
            default: null
        },
        variant: {
            type: String,
            default: null
        },
        dropup: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        right: {
            type: Boolean,
            default: false
        }
    },
    created: function created() {
        var this$1 = this;

        this.$root.$on('shown::dropdown', function (el) {
            if (el !== this$1) {
                this$1.visible = false;
            }
        });
    },
    watch: {
        visible: function visible(state, old) {
            if (state === old) {
                return; // Avoid duplicated emits
            }

            if (state) {
                this.$root.$emit('shown::dropdown', this);
            } else {
                this.$root.$emit('hidden::dropdown', this);
            }
        }
    },
    methods: {
        toggle: function toggle() {
            this.visible = !this.visible;
        },
        clickOutListener: function clickOutListener() {
            this.visible = false;
        },
        click: function click(e) {
            if (this.split) {
                this.$emit('click', e);
                this.$root.$emit('shown::dropdown', this);
            } else {
                this.toggle();
            }
        }
    }
};

var bLink = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.componentType,{tag:"a",attrs:{"active-class":_vm.activeClass,"to":_vm.to,"href":_vm.hrefString,"exact":_vm.exact},on:{"click":_vm.click}},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        componentType: function componentType() {
            return (this.$router && this.to) ? 'router-link' : 'a';
        },
        hrefString: function hrefString() {
            if (this.to) {
                return this.to.path || this.to;
            }
            return this.href;
        }
    },
    props: {
        activeClass: {
            type: String,
            default: 'active'
        },
        to: {
            type: [String, Object],
            default: null
        },
        href: {
            type: String
        },
        exact: {
            type: Boolean,
            default: false
        }
    },
    methods: {
        click: function click(e) {
            this.$emit('click', e);
            this.$root.$emit('shown::dropdown', this);
        }
    }
};

var dropdownItem = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.itemType,{tag:"a",staticClass:"dropdown-item",attrs:{"to":_vm.to,"href":_vm.hrefString},on:{"click":_vm.click}},[_vm._t("default")],2)},staticRenderFns: [],
    extends: bLink,
    computed: {
        itemType: function itemType() {
            return (this.href || this.to) ? this.componentType : 'button';
        }
    }
};

var dropdownSelect = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"dropdown-select",class:{open: _vm.show, dropdown: !_vm.dropup, dropup: _vm.dropup}},[_c('button',{class:['btn','dropdown',_vm.dropdownToggle,_vm.btnVariant,_vm.btnSize],attrs:{"id":_vm.id,"role":"button","aria-haspopup":"true","aria-expanded":"show","disabled":_vm.disabled},on:{"click":function($event){$event.preventDefault();_vm.toggle($event);}}},[_c('span',{staticClass:"checked-items",domProps:{"innerHTML":_vm._s(_vm.displayItem)}})]),_c('ul',{staticClass:"dropdown-menu",class:{'dropdown-menu-right' : _vm.position == 'right'},attrs:{"aria-labelledby":"dLabel"}},_vm._l((_vm.list),function(item){return _c('li',[_c('button',{staticClass:"dropdown-item",attrs:{"click":_vm.select(item)}},[_vm._v(_vm._s(item.text))])])}))])},staticRenderFns: [],
    replace: true,
    data: function data() {
        return {
            show: false,
            selected: false
        };
    },
    computed: {
        btnVariant: function btnVariant() {
            return !this.variant || this.variant === "default" ? "btn-secondary" : ("btn-" + (this.variant));
        },
        btnSize: function btnSize() {
            return !this.size || this.size === "default" ? "" : ("btn-" + (this.size));
        },
        dropdownToggle: function dropdownToggle() {
            return this.caret ? 'dropdown-toggle' : '';
        },
        displayItem: function displayItem() {
            // If zero show default message
            if ((this.returnObject && this.model && !this.model.text) || (!this.returnObject && this.model && this.model.length === 0) || this.forceDefault) {
                return this.defaultText;
            }

            // Show selected item
            if (this.returnObject && this.model && this.model.text) {
                return this.model.text;
            }

            // Show text that coresponds to the model value
            if (!this.returnObject && this.model) {
                var result = this.model || '';
                this.list.forEach(function (item) {
                    if (item.value === this.model) {
                        result = item.text;
                    }
                });
                return result;
            }

            return '';
        }
    },
    props: {
        id: {
            type: String
        },
        model: {
            required: false
        },
        list: {
            type: Array,
            default: [],
            required: true
        },
        caret: {
            type: Boolean,
            default: true
        },
        position: {
            type: String,
            default: 'left'
        },
        size: {
            type: String,
            default: ''
        },
        variant: {
            type: String,
            default: 'default'
        },
        defaultText: {
            type: String,
            default: 'Plase select one'
        },
        forceDefault: {
            type: Boolean,
            default: false
        },
        returnObject: {
            type: Boolean,
            default: false
        },
        dropup: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        }
    },
    methods: {
        toggle: function toggle(e) {
            // Hide an alert
            this.show = !this.show;
            // Dispatch an event from the current vm that propagates all the way up to its $root
            if (this.show) {
                this.$root.$emit('shown:dropdown', this.id);
                e.stopPropagation();
            } else {
                this.$root.$emit('hidden::dropdown', this.id);
            }
        },
        select: function select(item) {
            // We need to set empty model to make model watchers react to it
            if (this.returnObject) {
                this.model = item;
            } else {
                this.model = item.value;
            }
            this.show = false;
            // Dispatch an event from the current vm that propagates all the way up to its $root
            this.$root.$emit('selected::dropdown', this.id, this.model);
        }
    },
    created: function created() {
        var hub = this.$root;
        hub.$on('hide::dropdown', function () {
            this.show = false;
        });
    }
};

var form = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('form',{class:_vm.classObject},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return [
                this.inline ? 'form-inline' : ''
            ];
        }
    },
    props: {
        inline: {
            type: Boolean,
            default: false
        }
    }
};

var formFieldset = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['form-group','row',_vm.inputState]},[(_vm.label)?_c('label',{class:['col-form-label',_vm.labelLayout],attrs:{"for":_vm.target},domProps:{"innerHTML":_vm._s(_vm.label)}}):_vm._e(),_c('div',{ref:"content",class:_vm.inputLayout},[_vm._t("default"),(_vm.feedback)?_c('div',{staticClass:"form-text text-muted",domProps:{"innerHTML":_vm._s(_vm.feedback)}}):_vm._e(),(_vm.description)?_c('small',{staticClass:"form-text text-muted",domProps:{"innerHTML":_vm._s(_vm.description)}}):_vm._e()],2)])},staticRenderFns: [],
    data: function data() {
        return {
            target: null
        };
    },
    computed: {
        inputState: function inputState() {
            return this.state ? ("has-" + (this.state)) : '';
        },
        labelLayout: function labelLayout() {
            return this.horizontal ? ('col-sm-' + this.labelSize) : 'col-12';
        },
        inputLayout: function inputLayout() {
            return this.horizontal ? ('col-sm-' + (12 - this.labelSize)) : 'col-12';
        }
    },
    mounted: function mounted() {
        var content = this.$refs.content;
        if (!content) {
            return;
        }
        this.target = content.children[0].id;
    },
    props: {
        state: {
            type: String,
            default: null
        },
        horizontal: {
            type: Boolean,
            default: false
        },
        labelSize: {
            type: Number,
            default: 3
        },
        label: {
            type: String,
            default: null
        },
        description: {
            type: String,
            default: null
        },
        feedback: {
            type: String,
            default: null
        }
    }
};

var formMixin = {
    computed: {
        inputClass: function inputClass() {
            return [
                this.size ? ("form-control-" + (this.size)) : null,
                this.state ? ("form-control-" + (this.state)) : null
            ];
        },
        custom: function custom() {
            return !this.plain;
        }
    },
    props: {
        name: {
            type: String
        },
        id: {
            type: String,
            default: uniqueId
        },
        disabled: {
            type: Boolean
        },
        plain: {
            type: Boolean,
            default: false
        },
        state: {
            type: String
        },
        size: {
            type: String
        }
    }
};

var formCheckBoxMixin = {
    computed: {
        checkboxClass: function checkboxClass() {
            return {
                'custom-control': this.custom,
                'form-check-inline': this.inline
            };
        }
    }
};

var formCheckbox = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{class:[_vm.inputClass,_vm.checkboxClass,_vm.custom?'custom-checkbox':null]},[_c('input',{class:[_vm.custom?'custom-control-input':null],attrs:{"type":"checkbox","id":_vm.id,"name":_vm.name,"disabled":_vm.disabled},domProps:{"value":_vm.value,"checked":_vm.checked===_vm.value},on:{"change":function($event){_vm.$emit('change',$event.target.checked?_vm.value:_vm.uncheckedValue);}}}),_vm._v(" "),(_vm.custom)?_c('span',{staticClass:"custom-control-indicator"}):_vm._e(),_vm._v(" "),_c('span',{class:[_vm.custom?'custom-control-description':null]},[_vm._t("default")],2)])},staticRenderFns: [],
    mixins: [formMixin, formCheckBoxMixin],
    model: {
        prop: 'checked',
        event: 'change'
    },
    props: {
        value: {
            default: true
        },
        uncheckedValue: {
            default: false
        },
        checked: {
            default: true
        }
    }
};

var formOptions = {
    computed: {
        formOptions: function formOptions() {
            var options = this.options || {};

            if (Array.isArray(options)) {
                // Normalize flat arrays to object array
                options = options.map(function (option) {
                    if (typeof option === 'object') {
                        return option || {}; // Type of null is object
                    }

                    return {text: String(option), value: option || {}};
                });
            } else {
                // Normalize Objects to Array
                options = Object.keys(options).map(function (value) {
                    var option = options[value] || {};

                    // Resolve text
                    if (typeof option !== 'object') {
                        option = {text: String(option)};
                    }
                    // Resolve value
                    if (!option.value) {
                        option.value = value;
                    }
                    return option;
                });
            }

            return options;
        },
        selectedValue: function selectedValue() {
            var this$1 = this;

            var formOptions = this.formOptions;
            for (var i = 0; i < formOptions.length; i++) {
                if (formOptions[i].value === this$1.localValue) {
                    if (this$1.returnObject) {
                        return formOptions[i];
                    }
                    return formOptions[i].value;
                }
            }
        }
    },
    watch: {
        localValue: function localValue(value, old_value) {
            if (value === old_value) {
                return;
            }
            this.$emit('input', this.selectedValue);
        },
        value: function value(value$1, old_value) {
            if (value$1 === old_value) {
                return;
            }
            this.localValue = value$1;
        }
    }
};

var formRadio = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:[_vm.inputClass,this.stacked?'custom-controls-stacked':'']},_vm._l((_vm.formOptions),function(option){return _c('label',{class:[_vm.checkboxClass,_vm.custom?'custom-radio':null]},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.localValue),expression:"localValue"}],ref:"inputs",refInFor:true,class:_vm.custom?'custom-control-input':null,attrs:{"type":"radio","name":option.name,"id":option.id,"disabled":option.disabled},domProps:{"value":option.value,"checked":_vm._q(_vm.localValue,option.value)},on:{"__c":function($event){_vm.localValue=option.value;}}}),_vm._v(" "),(_vm.custom)?_c('span',{staticClass:"custom-control-indicator"}):_vm._e(),_vm._v(" "),_c('span',{class:_vm.custom?'custom-control-description':null,domProps:{"innerHTML":_vm._s(option.text)}})])}))},staticRenderFns: [],
    mixins: [formMixin, formCheckBoxMixin, formOptions],
    data: function data() {
        return {
            localValue: this.value
        };
    },
    computed: {
        inputState: function inputState() {
            return this.state ? ("has-" + (this.state)) : '';
        }
    },
    props: {
        value: {},
        options: {
            type: [Array, Object],
            default: null,
            required: true
        },
        stacked: {
            type: Boolean,
            default: false
        },
        returnObject: {
            type: Boolean,
            default: false
        }
    }
};

var formInput = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.textarea?'textarea':'input',{ref:"input",tag:"input",class:['form-control',_vm.inputClass],attrs:{"type":_vm.type,"name":_vm.name,"id":_vm.id || ('b_'+_vm._uid),"disabled":_vm.disabled,"rows":_vm.rows || _vm.rowsCount,"placeholder":_vm.placeholder},domProps:{"value":_vm.value},on:{"input":function($event){_vm.onInput($event.target.value);},"change":function($event){_vm.onChange($event.target.value);},"keyup":function($event){_vm.onKeyUp($event);},"focus":function($event){_vm.$emit('focus');},"blur":function($event){_vm.$emit('blur');}}})},staticRenderFns: [],
    mixins: [formMixin],
    computed: {
        rowsCount: function rowsCount() {
            return (this.value || '').split('\n').length;
        }
    },
    methods: {
        format: function format(value) {
            if (this.formatter) {
                var formattedValue = this.formatter(value);
                if (formattedValue !== value) {
                    value = formattedValue;
                    this.$refs.input.value = formattedValue;
                }
            }
            return value;
        },
        onInput: function onInput(value) {
            if (!this.lazyFormatter) {
                value = this.format(value);
            }
            this.$emit('input', value);
        },
        onChange: function onChange(value) {
            value = this.format(value);
            this.$emit('input', value);
            this.$emit('change', value);
        },
        onKeyUp: function onKeyUp(e) {
            this.$emit('keyup', e);
        }
    },
    props: {
        value: {
            default: null
        },
        type: {
            type: String,
            default: 'text'
        },
        placeholder: {
            type: String,
            default: null
        },
        rows: {
            type: Number,
            default: null
        },
        textarea: {
            type: Boolean,
            default: false
        },
        formatter: {
            type: Function
        },
        lazyFormatter: {
            type: Boolean,
            default: false
        }
    }
};

var formFile = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('label',{class:[_vm.custom?'custom-file':null,_vm.inputClass],on:{"dragover":function($event){$event.stopPropagation();$event.preventDefault();_vm.dragover($event);}}},[(_vm.dragging)?_c('span',{staticClass:"drop-here",attrs:{"data-drop":_vm.dropLabel},on:{"dragover":function($event){$event.stopPropagation();$event.preventDefault();_vm.dragover($event);},"drop":function($event){$event.stopPropagation();$event.preventDefault();_vm.drop($event);},"dragleave":function($event){$event.stopPropagation();$event.preventDefault();_vm.dragging=false;}}}):_vm._e(),_c('input',{ref:"input",staticClass:"custom-file-input",attrs:{"type":"file","name":_vm.name,"id":_vm.id,"disabled":_vm.disabled,"accept":_vm.accept,"multiple":_vm.multiple,"webkitdirectory":_vm.directory},on:{"change":_vm.onFileChange}}),_vm._v(" "),(_vm.custom)?_c('span',{class:['custom-file-control',_vm.dragging?'dragging':null,_vm.inputClass],attrs:{"data-choose":_vm.computedChooseLabel,"data-selected":_vm.selectedLabel}}):_vm._e()])},staticRenderFns: [],
    mixins: [formMixin],
    data: function data() {
        return {
            selectedFile: null,
            dragging: false
        };
    },
    computed: {
        selectedLabel: function selectedLabel() {
            if (!this.selectedFile || this.selectedFile.length === 0) {
                return this.placeholder || 'No file chosen';
            }

            if (this.multiple) {
                if (this.selectedFile.length === 1) {
                    return this.selectedFile[0].name;
                }

                return this.selectedFormat
                    .replace(':names', this.selectedFile.map(function (file) { return file.name; }).join(','))
                    .replace(':count', this.selectedFile.length);
            }

            return this.selectedFile.name;
        },
        computedChooseLabel: function computedChooseLabel() {
            return this.chooseLabel || (this.multiple ? 'Choose Files' : 'Choose File');
        }
    },
    watch: {
        selectedFile: function selectedFile(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }

            if (!newVal && this.multiple) {
                this.$emit('input', []);
            } else {
                this.$emit('input', newVal);
            }
        }
    },
    methods: {
        onFileChange: function onFileChange(e) {
            var this$1 = this;

            // Always emit original event
            this.$emit('change', e);

            // Check if special `items` prop is available on event (drop mode)
            // Can be disabled by setting no-traverse
            var items = e.dataTransfer && e.dataTransfer.items;
            if (items && !this.noTraverse) {
                var queue = [];
                for (var i = 0; i < items.length; i++) {
                    var item = items[i].webkitGetAsEntry();
                    if (item) {
                        queue.push(this$1.traverseFileTree(item));
                    }
                }
                Promise.all(queue).then(function (filesArr) {
                    this$1.setFiles(Array.prototype.concat.apply([], filesArr));
                });
                return;
            }

            // Normal handling
            this.setFiles(e.target.files || e.dataTransfer.files);
        },
        setFiles: function setFiles(files) {
            var this$1 = this;

            if (!files) {
                this.selectedFile = null;
                return;
            }

            if (!this.multiple) {
                this.selectedFile = files[0];
                return;
            }

            // Convert files to array
            var filesArray = [];
            for (var i = 0; i < files.length; i++) {
                if (files[i].type.match(this$1.accept)) {
                    filesArray.push(files[i]);
                }
            }

            this.selectedFile = filesArray;
        },
        dragover: function dragover(e) {
            if (this.noDrop) {
                return;
            }

            this.dragging = true;
            e.dataTransfer.dropEffect = 'copy';
        },
        drop: function drop(e) {
            if (this.noDrop) {
                return;
            }

            this.dragging = false;
            if (e.dataTransfer.files && e.dataTransfer.files.length > 0) {
                this.onFileChange(e);
            }
        },
        traverseFileTree: function traverseFileTree(item, path) {
            var this$1 = this;

            // Based on http://stackoverflow.com/questions/3590058
            return new Promise(function (resolve) {
                path = path || '';
                if (item.isFile) {
                    // Get file
                    item.file(function (file) {
                        file.$path = path; // Inject $path to file obj
                        resolve(file);
                    });
                } else if (item.isDirectory) {
                    // Get folder contents
                    item.createReader().readEntries(function (entries) {
                        var queue = [];
                        for (var i = 0; i < entries.length; i++) {
                            queue.push(this$1.traverseFileTree(entries[i], path + item.name + '/'));
                        }
                        Promise.all(queue).then(function (filesArr) {
                            resolve(Array.prototype.concat.apply([], filesArr));
                        });
                    });
                }
            });
        }
    },
    props: {
        accept: {
            type: String,
            default: ''
        },
        placeholder: {
            type: String,
            default: null
        },
        chooseLabel: {
            type: String,
            default: null
        },
        multiple: {
            type: Boolean,
            default: false
        },
        directory: {
            type: Boolean,
            default: false
        },
        noTraverse: {
            type: Boolean,
            default: false
        },
        selectedFormat: {
            type: String,
            default: ':count Files'
        },
        noDrop: {
            type: Boolean,
            default: false
        },
        dropLabel: {
            type: String,
            default: 'Drop files here'
        }
    }
};

var formSelect = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('select',{directives:[{name:"model",rawName:"v-model",value:(_vm.localValue),expression:"localValue"}],ref:"input",class:[_vm.inputClass,_vm.custom?'custom-select':null],attrs:{"name":_vm.name,"id":_vm.id,"disabled":_vm.disabled},on:{"change":function($event){var $$selectedVal = Array.prototype.filter.call($event.target.options,function(o){return o.selected}).map(function(o){var val = "_value" in o ? o._value : o.value;return val}); _vm.localValue=$event.target.multiple ? $$selectedVal : $$selectedVal[0];}}},_vm._l((_vm.formOptions),function(option){return _c('option',{attrs:{"disabled":option.disabled},domProps:{"value":option.value,"innerHTML":_vm._s(option.text)}})}))},staticRenderFns: [],
    mixins: [formMixin, formOptions],
    data: function data() {
        return {
            localValue: this.value
        };
    },
    props: {
        value: {},
        options: {
            type: [Array, Object],
            required: true
        },
        returnObject: {
            type: Boolean,
            default: false
        }
    }
};

var jumbotron = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{class:['jumbotron',_vm.fluid?'jumbotron-fluid':null]},[_c('div',{staticClass:"container"},[(_vm.header)?_c('h1',{staticClass:"display-3",domProps:{"innerHTML":_vm._s(_vm.header)}}):_vm._e(),(_vm.lead)?_c('p',{staticClass:"lead",domProps:{"innerHTML":_vm._s(_vm.lead)}}):_vm._e(),_vm._t("default")],2)])},staticRenderFns: [],
    replace: true,
    computed: {},
    props: {
        fluid: {
            type: Boolean,
            default: false
        },
        header: {
            type: String,
            default: null
        },
        lead: {
            type: String,
            default: null
        }
    }
};

var badge = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('span',{class:['badge',_vm.badgeVariant,_vm.badgePill]},[_vm._t("default")],2)},staticRenderFns: [],
    replace: true,
    computed: {
        badgeVariant: function badgeVariant() {
            return !this.variant || this.variant === "default" ? "badge-default" : ("badge-" + (this.variant));
        },
        badgePill: function badgePill() {
            return this.pill ? 'badge-pill' : '';
        }
    },
    props: {
        variant: {
            type: String,
            default: 'default'
        },
        pill: {
            type: Boolean,
            default: false
        }
    }
};

var listGroup = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.tag,{tag:"component",class:['list-group',_vm.flush?'list-group-flush':null]},[_vm._t("default")],2)},staticRenderFns: [],
    replace: true,
    props: {
        tag: {
            type: String,
            default: 'div'
        },
        flush: {
            type: Boolean,
            default: false
        }
    }
};

var actionTags = ['a', 'router-link', 'button', 'b-link'];

var listGroupItem = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.myTag,{ref:"item",tag:"component",class:_vm.classObject,attrs:{"to":_vm.to,"href":_vm.href}},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return [
                'list-group-item',
                this.listState,
                this.active ? 'active' : null,
                this.disabled ? 'disabled' : null,
                this.isAction ? 'list-group-item-action' : null
            ];
        },
        isAction: function isAction() {
            if (this.action === false) {
                return false;
            }
            return this.action || this.to || this.href || actionTags.indexOf(this.tag) !== -1;
        },
        listState: function listState() {
            return this.variant ? ("list-group-item-" + (this.variant)) : null;
        },
        myTag: function myTag() {
            if (this.tag) {
                return this.tag;
            }
            return (this.to || this.href) ? 'b-link' : 'div';
        }
    },
    props: {
        tag: {
            type: String,
            default: null
        },
        active: {
            type: Boolean,
            default: false
        },
        action: {
            type: Boolean,
            default: null
        },
        disabled: {
            type: Boolean,
            default: false
        },
        variant: {
            type: String,
            default: null
        },
        to: {
            type: String,
            default: null
        },
        href: {
            type: String,
            default: null
        }
    }
};

var media = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"media"},[(_vm.mediaHorizontalAlign == 'media-left')?_c('div',{class:[_vm.mediaHorizontalAlign,_vm.mediaVerticalAlign]},[_vm._t("aside")],2):_vm._e(),_c('div',{staticClass:"media-body"},[_vm._t("body")],2),(_vm.mediaHorizontalAlign == 'media-right')?_c('div',{class:[_vm.mediaHorizontalAlign,_vm.mediaVerticalAlign]},[_vm._t("aside")],2):_vm._e()])},staticRenderFns: [],
    replace: true,
    computed: {
        align: function align() {
            return this.position.split(' ');
        },
        mediaVerticalAlign: function mediaVerticalAlign() {
            var verticalAlign = this.align[0];
            return ("media-" + verticalAlign);
        },
        mediaHorizontalAlign: function mediaHorizontalAlign() {
            var horizontalAlign = this.align[1];
            return ("media-" + horizontalAlign);
        }
    },
    props: {
        position: {
            type: String,
            default: 'top left'
        }
    }
};

var modal = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('transition-group',{attrs:{"enter-class":"hidden","enter-to-class":"show","enter-active-class":"","leave-class":"show","leave-active-class":"","leave-to-class":"hidden"},on:{"after-enter":_vm.afterEnter}},[_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.visible),expression:"visible"}],key:"modal",class:['modal',{fade :_vm.fade}],attrs:{"id":_vm.id},on:{"click":function($event){_vm.onClickOut($event);}}},[_c('div',{class:['modal-dialog','modal-'+_vm.size]},[_c('div',{staticClass:"modal-content"},[(!_vm.hideHeader)?_c('div',{staticClass:"modal-header"},[_vm._t("modal-header",[_c('h5',{staticClass:"modal-title"},[_vm._t("modal-title",[_vm._v(_vm._s(_vm.title))])],2),_c('button',{staticClass:"close",attrs:{"type":"button","aria-label":"Close"},on:{"click":_vm.hide}},[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("×")])])])],2):_vm._e(),_c('div',{staticClass:"modal-body"},[_vm._t("default")],2),(!_vm.hideFooter)?_c('div',{staticClass:"modal-footer"},[_vm._t("modal-footer",[_c('b-btn',{attrs:{"variant":"secondary"},on:{"click":function($event){_vm.hide(false);}}},[_vm._v(_vm._s(_vm.closeTitle))]),_c('b-btn',{attrs:{"variant":"primary"},on:{"click":function($event){_vm.hide(true);}}},[_vm._v(_vm._s(_vm.okTitle))])])],2):_vm._e()])])]),(_vm.visible)?_c('div',{key:"modal-backdrop",class:['modal-backdrop',{fade: _vm.fade}]}):_vm._e()])],1)},staticRenderFns: [],
    data: function data() {
        return {
            visible: false
        };
    },
    computed: {
        body: function body() {
            if (typeof document !== 'undefined') {
                return document.querySelector('body');
            }
        }
    },
    props: {
        id: {
            type: String,
            default: null
        },
        title: {
            type: String,
            default: ''
        },
        size: {
            type: String,
            default: 'md'
        },
        fade: {
            type: Boolean,
            default: true
        },
        closeTitle: {
            type: String,
            default: 'Close'
        },
        okTitle: {
            type: String,
            default: 'OK'
        },
        closeOnBackdrop: {
            type: Boolean,
            default: true
        },
        hideHeader: {
            type: Boolean,
            default: false
        },
        hideFooter: {
            type: Boolean,
            default: false
        }
    },
    methods: {
        show: function show() {
            if (this.visible) {
                return;
            }
            this.visible = true;
            this.$root.$emit('shown::modal', this.id);
            this.body.classList.add('modal-open');
            this.$emit('shown');
        },
        hide: function hide(isOK) {
            if (!this.visible) {
                return;
            }
            this.visible = false;
            this.$root.$emit('hidden::modal', this.id);
            this.body.classList.remove('modal-open');

            this.$emit('hidden', isOK);

            if (isOK === true) {
                this.$emit('ok');
            } else if (isOK === false) {
                this.$emit('cancel');
            }
        },
        onClickOut: function onClickOut(e) {
            // If backdrop clicked, hide modal
            if (this.closeOnBackdrop && e.target.id && e.target.id === this.id) {
                this.hide();
            }
        },
        pressedButton: function pressedButton(e) {
            // If not visible don't do anything
            if (!this.visible) {
                return;
            }

            // Support for esc key press
            var key = e.which || e.keyCode;
            if (key === 27) { // 27 is esc
                this.hide();
            }
        },
        afterEnter: function afterEnter(el) {
            // Add show class to keep el showed just after transition is ended,
            // Because transition removes all used classes
            el.classList.add('show');
        }
    },
    created: function created() {
        var this$1 = this;

        this.$root.$on('show::modal', function (id) {
            if (id === this$1.id) {
                this$1.show();
            }
        });

        this.$root.$on('hide::modal', function (id) {
            if (id === this$1.id) {
                this$1.hide();
            }
        });
    },
    mounted: function mounted() {
        if (typeof document !== 'undefined') {
            document.addEventListener('keydown', this.pressedButton);
        }
    },
    destroyed: function destroyed() {
        if (typeof document !== 'undefined') {
            document.removeEventListener('keydown', this.pressedButton);
        }
    }
};

var nav = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c(_vm.type,{tag:"component",class:_vm.classObject},[_vm._t("default")],2)},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return {
                nav: true,
                'navbar-nav': this.isNavBar,
                'nav-tabs': this.tabs,
                'nav-pills': this.pills,
                'flex-column': this.vertical,
                'nav-fill': this.fill
            };
        }
    },
    props: {
        type: {
            type: String,
            default: 'ul'
        },
        fill: {
            type: Boolean,
            default: false
        },
        tabs: {
            type: Boolean,
            default: false
        },
        pills: {
            type: Boolean,
            default: false
        },
        vertical: {
            type: Boolean,
            default: false
        },
        isNavBar: {
            type: Boolean,
            default: false
        }
    }
};

var navItem = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('li',{staticClass:"nav-item",on:{"click":_vm.onclick}},[_c('b-link',{class:_vm.classObject,attrs:{"to":_vm.to,"href":_vm.href,"exact":_vm.exact}},[_vm._t("default")],2)],1)},staticRenderFns: [],
    components: {
        bLink: bLink
    },
    computed: {
        classObject: function classObject() {
            return [
                'nav-link',
                this.active ? 'active' : '',
                this.disabled ? 'disabled' : ''
            ];
        }
    },
    props: {
        active: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        to: {
            type: [String, Object]
        },
        href: {
            type: String
        },
        exact: {
            type: Boolean
        }
    },
    methods: {
        onclick: function onclick(e) {
            // Hide all drop-downs including navbar-toggle
            this.$root.$emit('shown::dropdown', this);
            this.$emit('click', e);
        }
    }
};

var navItemDropdown = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('li',{class:{'nav-item': true, show: _vm.visible,dropdown: !_vm.dropup, dropup: _vm.dropup}},[_c('a',{class:['nav-link', _vm.dropdownToggle],attrs:{"href":"","aria-haspopup":"true","aria-expanded":_vm.visible,"disabled":_vm.disabled},on:{"click":function($event){$event.stopPropagation();$event.preventDefault();_vm.toggle($event);}}},[_vm._t("text",[_vm._v(_vm._s(_vm.text))])],2),_c('div',{class:{'dropdown-menu': true, 'dropdown-menu-right': _vm.rightAlignment}},[_vm._t("default")],2)])},staticRenderFns: [],
    mixins: [
        clickOut
    ],
    data: function data() {
        return {
            visible: false
        };
    },
    computed: {
        dropdownToggle: function dropdownToggle() {
            return this.caret ? 'dropdown-toggle' : '';
        }
    },
    props: {
        caret: {
            type: Boolean,
            default: true
        },
        text: {
            type: String,
            default: ''
        },
        dropup: {
            type: Boolean,
            default: false
        },
        rightAlignment: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        class: ['class']
    },
    created: function created() {
        var this$1 = this;

        // To keep one dropdown opened at page
        this.$root.$on('shown::dropdown', function (el) {
            if (el !== this$1) {
                this$1.close();
            }
        });
    },
    watch: {
        visible: function visible(state, old) {
            if (state === old) {
                return; // Avoid duplicated emits
            }

            if (state) {
                this.$root.$emit('shown::dropdown', this);
            } else {
                this.$root.$emit('hidden::dropdown', this);
            }
        }
    },
    methods: {
        toggle: function toggle() {
            this.visible = !this.visible;
        },
        open: function open() {
            this.visible = true;
        },
        close: function close() {
            this.visible = false;
        },
        clickOutListener: function clickOutListener() {
            this.close();
        }
    }
};

var navToggle = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('button',{class:_vm.classObject,attrs:{"type":"button","aria-label":_vm.label},on:{"click":_vm.onclick}},[_c('span',{staticClass:"navbar-toggler-icon"})])},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return [
                'navbar-toggler',
                'navbar-toggler-' + this.position
            ];
        }
    },

    props: {
        label: {
            type: String,
            default: 'Toggle navigation'
        },
        position: {
            type: String,
            default: 'right'
        },
        target: {
            required: true
        }
    },

    methods: {
        onclick: function onclick() {
            var target = this.target;
            if (target.toggle) {
                target.toggle();
            }
            this.$root.$emit('collapse::toggle', this.target);
        }
    }
};

var navbar = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{class:_vm.classObject},[_vm._t("default")],2)},staticRenderFns: [],
    replace: true,
    computed: {
        classObject: function classObject() {
            return [
                'navbar',
                this.type ? ("navbar-" + (this.type)) : null,
                this.variant ? ("bg-" + (this.variant)) : null,
                this.fixed ? ("fixed-" + (this.fixed)) : null,
                this.sticky ? 'sticky-top' : null,
                this.toggleable ? ("navbar-toggleable-" + (this.toggleBreakpoint)) : null
            ];
        }
    },
    props: {
        type: {
            type: String,
            default: 'light'
        },
        variant: {
            type: String
        },
        toggleable: {
            type: Boolean,
            default: false
        },
        toggleBreakpoint: {
            type: String,
            default: 'sm'
        },
        fixed: {
            type: String
        },
        sticky: {
            type: String
        }
    }
};

var Pagination = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"btn-group pagination",attrs:{"role":"group","aria-label":"Pagination"}},[_c('button',{class:['btn','btn-'+_vm.secondaryVariant,_vm.btnSize],attrs:{"type":"button","disabled":_vm.currentPage == 1},on:{"click":function($event){$event.preventDefault();(_vm.currentPage == 1) ? _vm._return : _vm.currentPage--;}}},[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("«")])]),_vm._v(" "),_c('button',{directives:[{name:"show",rawName:"v-show",value:(_vm.showPrev),expression:"showPrev"}],class:['btn','btn-'+_vm.secondaryVariant,_vm.btnSize,_vm.currentPage === 1 ? 'active' : ''],attrs:{"type":"button"},on:{"click":function($event){$event.preventDefault();_vm.currentPage = 1;}}},[_vm._v("1")]),_vm._v(" "),_c('span',{directives:[{name:"show",rawName:"v-show",value:(_vm.showPrev),expression:"showPrev"}],class:['btn','btn-'+_vm.secondaryVariant,_vm.btnSize]},[_vm._v("...")]),_vm._v(" "),_vm._l((_vm.pageLinks),function(item,index){return _c('button',{class:['btn', _vm.btnSize, _vm.btnVariant(index), index + _vm.diff === _vm.currentPage ? 'active' : '', index + _vm.diff !== _vm.currentPage ? 'hidden-xs-down' : ''],attrs:{"type":"button"},on:{"click":function($event){$event.preventDefault();_vm.currentPage = index + _vm.diff;}}},[_vm._v(_vm._s(index + _vm.diff))])}),_vm._v(" "),_c('span',{directives:[{name:"show",rawName:"v-show",value:(_vm.showNext),expression:"showNext"}],class:['btn','btn-'+_vm.secondaryVariant,_vm.btnSize]},[_vm._v("...")]),_vm._v(" "),_c('button',{directives:[{name:"show",rawName:"v-show",value:(_vm.showNext),expression:"showNext"}],class:['btn','btn-'+_vm.secondaryVariant,_vm.btnSize,_vm.numberOfPages === _vm.currentPage ? 'active' : ''],attrs:{"type":"button"},on:{"click":function($event){$event.preventDefault();_vm.currentPage = _vm.numberOfPages;}}},[_vm._v(_vm._s(_vm.numberOfPages))]),_vm._v(" "),_c('button',{class:['btn','btn-'+_vm.secondaryVariant,_vm.btnSize],attrs:{"type":"button","disabled":_vm.currentPage == _vm.numberOfPages},on:{"click":function($event){$event.preventDefault();(_vm.currentPage == _vm.numberOfPages) ? _vm._return : _vm.currentPage++;}}},[_c('span',{attrs:{"aria-hidden":"true"}},[_vm._v("»")])])],2)},staticRenderFns: [],
    data: function data() {
        return {
            diff: 1,
            showPrev: false,
            showNext: false,
            currentPage: this.value
        };
    },
    computed: {
        numberOfPages: function numberOfPages() {
            var result = Math.ceil(this.totalRows / this.perPage);
            return (result < 1) ? 1 : result;
        },
        btnSize: function btnSize() {
            return !this.size || this.size === "default" ? "" : ("btn-" + (this.size));
        },
        pageLinks: function pageLinks() {
            var result = this.limit;
            if (this.currentPage > this.numberOfPages) {
                this.currentPage = 1;
            }
            this.diff = 1;
            this.showPrev = false;
            this.showNext = false;
            // If less pages than limit just show this pages
            if (this.numberOfPages <= this.limit) {
                return this.numberOfPages;
            }
            // If at the beggining of the list or at the end show full number of pages within limit - 2
            // -2 is reserves space for two buttons: "..." and "first/last button"
            if (this.currentPage <= this.limit - 2) {
                this.diff = 1;
                this.showNext = true;
                result = this.limit - 2;
            }
            // At the end of the range
            if (this.currentPage > this.numberOfPages - this.limit + 2) {
                this.diff = this.numberOfPages - this.limit + 3;
                this.showPrev = true;
                result = this.limit - 2;
            }
            // If somehere in the middle show just limit - 4 links in the middle and one button on the left with "..." and on button on the right preceeded with "..."
            if (this.currentPage >= this.limit - 2 && this.currentPage <= this.numberOfPages - this.limit + 2) {
                this.diff = this.currentPage - 1;
                this.showPrev = true;
                this.showNext = true;
                result = this.limit - 4;
            }
            return result;
        }
    },
    methods: {
        btnVariant: function btnVariant(index) {
            return (index + this.diff === this.currentPage) ? ("btn-" + (this.variant)) : ("btn-" + (this.secondaryVariant));
        },
        _return: function _return() {

        }
    },
    watch: {
        currentPage: function currentPage(newPage) {
            this.$emit('input', newPage);
        },
        value: function value(newValue, oldValue) {
            if (newValue !== oldValue) {
                this.currentPage = newValue;
            }
        }
    },
    props: {
        value: {
            type: Number,
            default: 1
        },
        limit: {
            type: Number,
            default: 7
        },
        perPage: {
            type: Number,
            default: 20
        },
        totalRows: {
            type: Number,
            default: 20
        },
        size: {
            type: String,
            default: 'md'
        },
        variant: {
            type: String,
            default: 'primary'
        },
        secondaryVariant: {
            type: String,
            default: 'secondary'
        }
    }
};

// Controls which events are mapped for each named trigger, and the expected popover behavior for each.
var triggerListeners = {
    click: {click: 'toggle'},
    hover: {mouseenter: 'show', mouseleave: 'hide'},
    focus: {focus: 'show', blur: 'hide'}
};

var popover = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('span',{ref:"trigger"},[_vm._t("default")],2),_c('div',{ref:"popover",class:['popover',_vm.popoverAlignment],style:(_vm.popoverStyle),attrs:{"tabindex":"-1"},on:{"focus":function($event){_vm.$emit('focus');},"blur":function($event){_vm.$emit('blur');}}},[_c('div',{staticClass:"popover-arrow"}),(_vm.title)?_c('h3',{staticClass:"popover-title",domProps:{"innerHTML":_vm._s(_vm.title)}}):_vm._e(),_c('div',{staticClass:"popover-content"},[_c('div',{staticClass:"popover-content-wrapper"},[_vm._t("content",[_c('span',{domProps:{"innerHTML":_vm._s(_vm.content)}})])],2)])])])},staticRenderFns: [],
    props: {
        placement: {
            type: String,
            default: 'top',
            validator: function validator(value) {
                return ['top', 'bottom', 'left', 'right'].indexOf(value) !== -1;
            }
        },
        triggers: {
            type: [Boolean, String, Array],
            default: function () { return ['click', 'focus']; },
            validator: function validator(value) {
                // Allow falsy value to disable all event triggers (equivalent to 'manual') in Bootstrap 4
                if (value === false || value === '') {
                    return true;
                } else if (typeof value === 'string') {
                    return Object.keys(triggerListeners).indexOf(value) !== -1;
                } else if (Array.isArray(value)) {
                    var keys = Object.keys(triggerListeners);
                    value.forEach(function (item) {
                        if (keys.indexOf(item) === -1) {
                            return false;
                        }
                    });
                    return true;
                }
                return false;
            }
        },
        title: {
            type: String,
            default: ''
        },
        content: {
            type: String,
            default: ''
        },
        show: {
            type: Boolean,
            default: false
        },
        constraints: {
            type: Array,
            default: function default$1() {
                return [];
            }
        },
        offset: {
            type: String,
            default: '0 0',
            validator: function validator(value) {
                return /^(\d+\s\d+)$/.test(value);
            }
        },
        delay: {
            type: [Number, Object],
            default: 0,
            validator: function validator(value) {
                if (typeof value === 'number') {
                    return value >= 0;
                } else if (value !== null && typeof value === 'object') {
                    return typeof value.show === 'number' &&
                        typeof value.hide === 'number' &&
                        value.show >= 0 &&
                        value.hide >= 0;
                }

                return false;
            }
        },
        debounce: {
            type: [Number],
            default: 300,
            validator: function validator(value) {
                return value >= 0;
            }
        },
        popoverStyle: {
            type: Object,
            default: null
        }
    },

    data: function data() {
        return {
            showState: this.show,
            lastEvent: null
        };
    },

    computed: {
        popoverAlignment: function popoverAlignment() {
            return !this.placement || this.placement === "default" ? "popover-top" : ("popover-" + (this.placement));
        },

        normalizedTriggers: function normalizedTriggers() {
            if (this.triggers === false) {
                return [];
            } else if (typeof this.triggers === 'string') {
                return [this.triggers];
            }
            return this.triggers;
        },

        placementParameters: function placementParameters() {
            switch (this.placement) {
                case 'bottom':
                    return {
                        attachment: 'top center',
                        targetAttachment: 'bottom center'
                    };
                case 'left':
                    return {
                        attachment: 'middle right',
                        targetAttachment: 'middle left'
                    };
                case 'right':
                    return {
                        attachment: 'middle left',
                        targetAttachment: 'middle right'
                    };
                default:
                    return {
                        attachment: 'bottom center',
                        targetAttachment: 'top center'
                    };
            }
        },

        useDebounce: function useDebounce() {
            return this.normalizedTriggers.length > 1;
        },

        tetherOptions: function tetherOptions() {
            return {
                element: this._popover,
                target: this._trigger,
                offset: this.offset,
                constraints: this.constraints,
                attachment: this.placementParameters.attachment,
                targetAttachment: this.placementParameters.targetAttachment
            };
        }
    },

    watch: {
        /**
         * Propagate 'show' property change
         * @param  {Boolean} newShow
         */
        show: function show(newShow) {
            this.showState = newShow;
        },

        /**
         * Affect 'show' state in response to status change
         * @param  {Boolean} newShowState
         * @param oldShowState
         */
        showState: function showState(newShowState, oldShowState) {
            var this$1 = this;

            if (newShowState === oldShowState) {
                return;
            }

            clearTimeout(this._timeout);

            this._timeout = setTimeout(function () {
                this$1.$emit('showChange', newShowState);
                if (newShowState) {
                    this$1.showPopover();
                } else {
                    this$1.hidePopover();
                }
            }, this.getDelay(newShowState));
        },

        normalizedTriggers: function normalizedTriggers(newTriggers, oldTriggers) {
            this.updateListeners(newTriggers, oldTriggers);
        },

        placement: function placement() {
            this.setOptions();
        },

        offset: function offset() {
            this.setOptions();
        },

        constraints: function constraints() {
            this.setOptions();
        },

        content: function content() {
            this.refreshPosition();
        },

        title: function title() {
            this.refreshPosition();
        }
    },

    methods: {
        /**
         * Display popover and fire event
         */
        showPopover: function showPopover() {
            if (this.showState === true) {
                this.hidePopover();
            }

            this.showState = true;

            // Let tether do the magic, after element is shown
            this._popover.style.display = 'block';
            this._tether = new Tether(this.tetherOptions);

            // Make sure the popup is rendered in the correct location
            this.refreshPosition();

            this.$root.$emit('shown::popover');
        },

        /**
         * Update tether options
         */
        setOptions: function setOptions() {
            if (this._tether) {
                this._tether.setOptions(this.tetherOptions);
            }
        },

        /**
         * Refresh the Popover position in order to respond to changes
         */
        refreshPosition: function refreshPosition() {
            var this$1 = this;

            if (this._tether) {
                this.$nextTick(function () {
                    this$1._tether.position();
                });
            }
        },

        /**
         * Hide popover and fire event
         */
        hidePopover: function hidePopover() {
            this.showState = false;
            this._popover.style.display = 'none';
            this.$root.$emit('hidden::popover');

            if (this._tether) {
                this._tether.destroy();
                this._tether = null;
            }
        },

        /**
         * Get the currently applicable popover delay
         * @returns Number
         */
        getDelay: function getDelay(state) {
            if (typeof this.delay === 'object') {
                return state ? this.delay.show : this.delay.hide;
            }

            return this.delay;
        },

        /**
         * Handle multiple event triggers
         * @param  {Object} e
         */
        eventHandler: function eventHandler(e) {
            var this$1 = this;

            // If this event is right after a previous successful event, ignore it
            if (this.useDebounce && this.debounce > 0 && this.lastEvent !== null && e.timeStamp <= this.lastEvent + this.debounce) {
                return;
            }

            // Look up the expected popover action for the event
            // eslint-disable-next-line guard-for-in
            for (var trigger in triggerListeners) {
                for (var event in triggerListeners[trigger]) {
                    if (event === e.type) {
                        var action = triggerListeners[trigger][event];

                        // If the expected event action is the opposite of the current state, allow it
                        if (action === 'toggle' || (this$1.showState && action === 'hide') || (!this$1.showState && action === 'show')) {
                            this$1.showState = !this$1.showState;
                            this$1.lastEvent = e.timeStamp;
                        }
                        return;
                    }
                }
            }
        },

        /**
         * Study the 'triggers' component property and apply all selected triggers
         * @param {Array} triggers
         * @param {Array} appliedTriggers
         */
        updateListeners: function updateListeners(triggers, appliedTriggers) {
            var this$1 = this;
            if ( appliedTriggers === void 0 ) appliedTriggers = [];

            var newTriggers = [];
            var removeTriggers = [];

            // Look for new events not yet mapped (all of them on first load)
            triggers.forEach(function (item) {
                if (appliedTriggers.indexOf(item) === -1) {
                    newTriggers.push(item);
                }
            });

            // Disable any removed event triggers
            appliedTriggers.forEach(function (item) {
                if (triggers.indexOf(item) === -1) {
                    removeTriggers.push(item);
                }
            });

            // Apply trigger mapping changes
            newTriggers.forEach(function (item) { return this$1.addListener(item); });
            removeTriggers.forEach(function (item) { return this$1.removeListener(item); });
        },

        /**
         * Add all event hooks for the given trigger
         * @param {String} trigger
         */
        addListener: function addListener(trigger) {
            var this$1 = this;

            // eslint-disable-next-line guard-for-in
            for (var item in triggerListeners[trigger]) {
                this$1._trigger.addEventListener(item, function (e) { return this$1.eventHandler(e); });
            }
        },

        /**
         * Remove all event hooks for the given trigger
         * @param {String} trigger
         */
        removeListener: function removeListener(trigger) {
            var this$1 = this;

            // eslint-disable-next-line guard-for-in
            for (var item in triggerListeners[trigger]) {
                this$1._trigger.removeEventListener(item, function (e) { return this$1.eventHandler(e); });
            }
        },

        /**
         * Cleanup component listeners
         */
        cleanup: function cleanup() {
            var this$1 = this;

            // Remove all event listeners
            // eslint-disable-next-line guard-for-in
            for (var trigger in this$1.normalizedTriggers) {
                this$1.removeListener(trigger);
            }

            clearTimeout(this._timeout);
            this._timeout = null;
            this.hidePopover();
        }
    },

    created: function created() {
        var this$1 = this;

        var hub = this.$root;
        hub.$on('hide::popover', function () {
            this$1.showState = false;
        });

        // Workaround to resolve issues like #151
        if (this.$router) {
            this.$router.beforeEach(function (to, from, next) {
                next();
                this$1.cleanup();
            });
        }

        hub.$on('hide::modal', function () {
            this$1.cleanup();
        });
    },

    mounted: function mounted() {
        // Configure tether
        this._trigger = this.$refs.trigger.children[0];
        this._popover = this.$refs.popover;
        this._popover.style.display = 'none';
        this._timeout = 0;

        // Add listeners for specified triggers and complementary click event
        this.updateListeners(this.normalizedTriggers);

        // Display popover if prop is set on load
        if (this.showState) {
            this.showPopover();
        }
    },

    beforeDestroy: function beforeDestroy() {
        this.cleanup();
    }
};

var progress = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"progress"},[_c('transition',[_c('div',{class:_vm.classObject,style:(_vm.styleObject),attrs:{"role":"progressbar","aria-valuenow":_vm.value,"aria-valuemin":0,"aria-valuemax":_vm.max}},[_vm._t("default",[(_vm.showProgress)?[_vm._v(_vm._s(_vm.progress)+"%")]:(_vm.showValue)?[_vm._v(_vm._s(_vm.value))]:_vm._e()])],2)])],1)},staticRenderFns: [],
    computed: {
        classObject: function classObject() {
            return [
                'progress-bar',
                this.progressVariant,
                (this.striped || this.animated) ? 'progress-bar-striped' : '',
                this.animated ? 'progress-bar-animated' : ''
            ];
        },
        styleObject: function styleObject() {
            return {
                width: this.progress + '%'
            };
        },
        progressVariant: function progressVariant() {
            return this.variant ? ("bg-" + (this.variant)) : null;
        },
        progress: function progress() {
            var p = Math.pow(10, this.precision);
            return Math.round((100 * p * this.value) / this.max) / p;
        }
    },
    props: {
        striped: {
            type: Boolean,
            default: false
        },
        animated: {
            type: Boolean,
            default: false
        },
        precision: {
            type: Number,
            default: 0
        },
        value: {
            type: Number,
            default: 0
        },
        max: {
            type: Number,
            default: 100
        },
        variant: {
            type: String,
            default: null
        },
        showProgress: {
            type: Boolean,
            default: false
        },
        showValue: {
            type: Boolean,
            default: false
        }
    }
};

var toString = function (v) {
    if (!v) {
        return '';
    }

    if (v instanceof Object) {
        return Object.keys(v).map(function (k) { return toString(v[k]); }).join(' ');
    }

    return String(v);
};

var defaultSortCompare = function (a, b, sortBy) {
    return toString(a[sortBy]).localeCompare(toString(b[sortBy]), undefined, {numeric: true});
};

var table = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('table',{class:['table',_vm.striped?'table-striped':'',_vm.hover?'table-hover':'']},[_c('thead',[_c('tr',_vm._l((_vm.fields),function(field,key){return _c('th',{class:[field.sortable?'sorting':null,_vm.sortBy===key?'sorting_'+(_vm.sortDesc?'desc':'asc'):'',field.class?field.class:null],domProps:{"innerHTML":_vm._s(field.label)},on:{"click":function($event){_vm.headClick(field,key);}}})}))]),_c('tbody',_vm._l((_vm._items),function(item,index){return _c('tr',{key:_vm.items_key,class:[item.state?'table-'+item.state:null]},_vm._l((_vm.fields),function(field,key){return _c('td',{class:[field.class?field.class:null]},[_vm._t(key,[_vm._v(_vm._s(item[key]))],{value:item[key],item:item,index:index})],2)}))}))])},staticRenderFns: [],
    components: {bPagination: Pagination},

    data: function data() {
        return {
            sortBy: null,
            sortDesc: true
        };
    },

    props: {
        items: {
            type: Array,
            default: function () { return []; }
        },
        fields: {
            type: Object,
            default: function () {
            }
        },
        striped: {
            type: Boolean,
            default: false
        },
        hover: {
            type: Boolean,
            default: false
        },
        perPage: {
            type: Number,
            default: null
        },
        items_key: {
            type: String,
            default: null
        },
        currentPage: {
            type: Number,
            default: 1
        },
        filter: {
            type: [String, RegExp, Function],
            default: null
        },
        sortCompare: {
            type: Function,
            default: null
        },
        itemsProvider: {
            type: Function,
            default: null
        }
    },

    computed: {
        _items: function _items() {
            var this$1 = this;

            if (!this.items) {
                return [];
            }

            if (this.itemsProvider) {
                return this.itemsProvider(this);
            }

            var items = this.items;

            // Apply filter
            if (this.filter) {
                if (this.filter instanceof Function) {
                    items = items.filter(this.filter);
                } else {
                    var regex;
                    if (this.filter instanceof RegExp) {
                        regex = this.filter;
                    } else {
                        regex = new RegExp('.*' + this.filter + '.*', 'ig');
                    }
                    items = items.filter(function (item) { return regex.test(toString(item)); });
                }
            }

            // Apply Sort
            var sortCompare = this.sortCompare || defaultSortCompare;
            if (this.sortBy) {
                items = items.sort(function (a, b) {
                    var r = sortCompare(a, b, this$1.sortBy);
                    return this$1.sortDesc ? r : r * -1;
                });
            }

            // Apply pagination
            if (this.perPage) {
                items = items.slice((this.currentPage - 1) * this.perPage, this.currentPage * this.perPage);
            }

            return items;
        }
    },
    methods: {
        headClick: function headClick(field, key) {
            if (!field.sortable) {
                this.sortBy = null;
                return;
            }

            if (key === this.sortBy) {
                this.sortDesc = !this.sortDesc;
            }

            this.sortBy = key;
        }
    }
};

/**
 * Observe a DOM element changes, falls back to eventListener mode
 * @param {Element} el The DOM element to observe
 * @param {Function} callback callback to be called on change
 * @param {object} [opts={childList: true, subtree: true}] observe options
 * @see http://stackoverflow.com/questions/3219758
 */
function observeDOM(el, callback, opts) {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var eventListenerSupported = window.addEventListener;

    if (MutationObserver) {
        // Define a new observer
        var obs = new MutationObserver(function (mutations) {
            if (mutations[0].addedNodes.length > 0 || mutations[0].removedNodes.length > 0) {
                callback();
            }
        });

        // Have the observer observe foo for changes in children
        obs.observe(el, Object.assign({childList: true, subtree: true}, opts));
    } else if (eventListenerSupported) {
        el.addEventListener('DOMNodeInserted', callback, false);
        el.addEventListener('DOMNodeRemoved', callback, false);
    }
}

var tabs = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"tabs"},[_c('div',{class:{'card-header': _vm.card}},[_c('ul',{class:['nav','nav-' + _vm.navStyle, _vm.card? 'card-header-'+_vm.navStyle: null]},[_vm._l((_vm.tabs),function(tab,index){return _c('li',{staticClass:"nav-item"},[_c('a',{class:['nav-link',{small: _vm.small, active: tab.localActive, disabled: tab.disabled}],attrs:{"href":tab.href},domProps:{"innerHTML":_vm._s(tab.title)},on:{"click":function($event){$event.preventDefault();$event.stopPropagation();_vm.setTab(index);}}})])}),_vm._t("tabs")],2)]),_c('div',{class:['tab-content',{'card-block': _vm.card}]},[_vm._t("default"),(!_vm.tabs || !_vm.tabs.length)?_vm._t("empty"):_vm._e()],2)])},staticRenderFns: [],
    data: function data() {
        return {
            currentTab: this.value || 0,
            tabs: []
        };
    },
    props: {
        noFade: {
            type: Boolean,
            default: false
        },
        card: {
            type: Boolean,
            default: false
        },
        small: {
            type: Boolean,
            default: false
        },
        value: {
            type: Number,
            default: 0
        },
        pills: {
            type: Boolean,
            default: false
        },
        lazy: {
            type: Boolean,
            default: false
        }
    },
    watch: {
        currentTab: function currentTab(val, old) {
            if (val === old) {
                return;
            }

            this.$root.$emit('changed::tab', this, val, this.tabs[val]);
            this.$emit('input', val);
        },
        value: function value(val, old) {
            if (val === old) {
                return;
            }

            this.setTab(val);
        },
        fade: function fade(val, old) {
            var this$1 = this;

            if (val === old) {
                return;
            }

            this.tabs.forEach(function (item) {
                this$1.$set(item, 'fade', val);
            });
        }
    },
    computed: {
        fade: function fade() {
            return !this.noFade;
        },
        navStyle: function navStyle() {
            return this.pills ? 'pills' : 'tabs';
        }
    },
    methods: {
        /**
         * Move to next tab
         */
        nextTab: function nextTab() {
            this.setTab(this.currentTab + 1);
        },

        /**
         * Move to previous tab
         */
        previousTab: function previousTab() {
            this.setTab(this.currentTab - 1);
        },

        /**
         * Set active tab on the tabs collection and the child 'tab' component
         */
        setTab: function setTab(index, force) {
            // Prevent setting same tab!
            if (!force && index === this.currentTab) {
                return;
            }

            var tab = this.tabs[index];

            // Don't go beyond indexes!
            if (!tab) {
                return;
            }

            // Ignore disabled
            if (tab.disabled) {
                return;
            }

            // Deactivate previous active tab
            if (this.tabs[this.currentTab]) {
                this.$set(this.tabs[this.currentTab], 'localActive', false);
            }

            // Set new tab as active
            this.$set(tab, 'localActive', true);

            // Update currentTab
            this.currentTab = index;
        },

        /**
         * Dynamically update tabs
         */
        _updateTabs: function _updateTabs() {
            var this$1 = this;

            // Probe tabs
            if (this.$slots.default) {
                this.tabs = this.$slots.default.filter(function (tab) { return tab.componentInstance || false; })
                    .map(function (tab) { return tab.componentInstance; });
            } else {
                this.tabs = [];
            }

            this.tabs.forEach(function (tab) {
                this$1.$set(tab, 'fade', this$1.fade);
                this$1.$set(tab, 'lazy', this$1.lazy);
            });

            // Set initial active tab
            var tabIndex = this.currentTab;

            this.tabs.forEach(function (tab, index) {
                if (tab.active) {
                    tabIndex = index;
                }
            });

            this.setTab(tabIndex, true);
        },

        /**
         * Wait for next tick so we can ensure DOM is updated before we inspect it
         */
        updateTabs: function updateTabs() {
            var this$1 = this;

            this.$nextTick(function () {
                this$1._updateTabs();
            });
        }
    },
    mounted: function mounted() {
        this.updateTabs();

        // Observe Child changes so we can notify tabs change
        observeDOM(this.$el, this.updateTabs.bind(this));
    }
};

var tab = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('transition',{attrs:{"enter-to-class":"show","leave-class":"show","mode":"out-in"},on:{"after-enter":_vm.afterEnter}},[(_vm.localActive || !_vm.lazy)?_c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.localActive || _vm.lazy),expression:"localActive || lazy"}],ref:"panel",staticClass:"tab-pane",class:[{fade: _vm.fade, disabled: _vm.disabled, active: _vm.localActive}],attrs:{"role":"tabpanel"}},[_vm._t("default")],2):_vm._e()])},staticRenderFns: [],
    methods: {
        afterEnter: function afterEnter(el) {
            el.classList.add('show');
        }
    },
    data: function data() {
        return {
            fade: false,
            localActive: false,
            lazy: true
        };
    },
    props: {
        id: {
            type: String,
            default: ''
        },
        title: {
            type: String,
            default: ''
        },
        disabled: {
            type: Boolean,
            default: false
        },
        active: {
            type: Boolean,
            default: false
        },
        href: {
            type: String,
            default: '#'
        }
    }
};

var tooltip = {render: function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('span',{ref:"trigger"},[_vm._t("default")],2),_c('div',{ref:"popover",class:['tooltip','tooltip-' + this.placement],style:({opacity:_vm.showState?1:0}),attrs:{"tabindex":"-1"},on:{"focus":function($event){_vm.$emit('focus');},"blur":function($event){_vm.$emit('blur');}}},[_c('div',{staticClass:"tooltip-inner"},[_vm._t("content",[_c('span',{domProps:{"innerHTML":_vm._s(_vm.content || _vm.title)}})])],2)])])},staticRenderFns: [],
    extends: popover,
    props: {
        triggers: {
            type: [Boolean, String, Array],
            default: 'hover'
        }
    }
};



var components = Object.freeze({
	bAlert: alert,
	bBreadcrumb: breadcrumb,
	bButtonCheckbox: buttonCheckbox,
	bButtonGroup: buttonGroup,
	bButtonRadio: buttonRadio,
	bButton: bButton,
	bBtn: bButton,
	bCard: card,
	bCardGroup: cardGroup,
	bDropdown: dropdown,
	bDropdownItem: dropdownItem,
	bDropdownSelect: dropdownSelect,
	bForm: form,
	bFormCheckbox: formCheckbox,
	bFormFieldset: formFieldset,
	bFormFile: formFile,
	bFormRadio: formRadio,
	bFormInput: formInput,
	bFormSelect: formSelect,
	bJumbotron: jumbotron,
	bBadge: badge,
	bMedia: media,
	bModal: modal,
	bNavbar: navbar,
	bPagination: Pagination,
	bPopover: popover,
	bProgress: progress,
	bTable: table,
	bTooltip: tooltip,
	bTab: tab,
	bTabs: tabs,
	bNav: nav,
	bNavItem: navItem,
	bNavItemDropdown: navItemDropdown,
	bNavToggle: navToggle,
	bListGroupItem: listGroupItem,
	bListGroup: listGroup,
	bSlide: carouselSlide,
	bCarousel: carousel,
	bCollapse: collapse,
	bLink: bLink
});

var all_listen_types = {hover: true, click: true, focus: true};

function targets(el, binding, listen_types, fn) {
    var vm = el.__vue__;

    if (!vm) {
        console.warn('__vue__ is not available on element', el);
        return;
    }

    var targets = Object.keys(binding.modifiers || {})
        .filter(function (t) { return !all_listen_types[t]; });

    if (binding.value) {
        targets.push(binding.value);
    }

    var listener = function () {
        fn({targets: targets, vm: vm});
    };

    Object.keys(all_listen_types).forEach(function (type) {
        if (listen_types[type] || binding.modifiers[type]) {
            console.log(type);
            el.addEventListener(type, listener);
        }
    });
}

var listen_types = {click: true};

var toggle = {
    bind: function bind(el, binding) {
        targets(el, binding, listen_types, function (ref) {
            var targets$$1 = ref.targets;
            var vm = ref.vm;

            targets$$1.forEach(function (target) {
                vm.$root.$emit('collapse::toggle', target);
            });
        });
    }
};

var listen_types$1 = {click: true};

var modal$1 = {
    bind: function bind(el, binding) {
        targets(el, binding, listen_types$1, function (ref) {
            var targets$$1 = ref.targets;
            var vm = ref.vm;

            targets$$1.forEach(function (target) {
                vm.$root.$emit('show::modal', target);
            });
        });
    }
};



var directives = Object.freeze({
	bToggle: toggle,
	bModal: modal$1
});

/* eslint-disable no-var, no-undef, guard-for-in, object-shorthand */

var VuePlugin = {
    install: function (Vue) {
        if (Vue._bootstrap_vue_installed) {
            return;
        }

        Vue._bootstrap_vue_installed = true;

        // Register components
        for (var component in components) {
            Vue.component(component, components[component]);
        }

        // Register directives
        for (var directive in directives) {
            Vue.directive(directive, directives[directive]);
        }
    }
};

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(VuePlugin);
}

return VuePlugin;

})));
//# sourceMappingURL=bootstrap-vue.js.map
