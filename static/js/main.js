// Vue-Resource设置
Vue.http.options.emulateHTTP = true;

// 获得URL参数
function getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
    var r = window.location.search.substr(1).match(reg);
    console.log(window.location.search.substr(1));
    if (r != null) {
        return unescape(r[2]);
    }
    return null;
}

var a = 123456;

async function getAddress() {

}

//导航条对象
var nav = new Vue({
        // 绑定到这个id的html元素
        el: '#nav',
        // 对象的属性
        data: {
            isLogin: false,
            username: '游客'
        },
        // 对象的方法
        methods: {
            logout: function() {
                // GET 
                this.$http.get('http://localhost:8081/api/logout', { credentials: true }).then(response => {
                    // success callback
                    console.log(response);
                    var data = response.body;
                    if (data.id == 0) {
                        //退出成功，刷新页面
                        window.location.reload();
                    }
                }, response => {
                    // error callback
                });
            }
        },
        // 对象的初始化方法
        created: function() {
            window.a = 654321;
            // GET 
            this.$http.get('http://localhost:8081/api/nav', { credentials: true }).then(function(response) {
                // success callback
                console.log(response);
                var data = response.body.message;
                this.isLogin = data.isLogin;
                window.isLogin = data.isLogin;
                window.uid = data.uid;
                this.username = data.username; //实际是email
                window.username = data.username;
            }, function(response) {
                // error callback
            });
        }
    })
    // 个人信息
var userinfo = new Vue({
        el: '#userinfo',
        data: {
            email: '',
            uid: '',
            username: '',
            emailstatus: '',
            status: '',
            exp: '',
            gold: '',
            u: '',
            e: '',
            p: ''
        },
        methods: {
            edituserinfo: function() {
                this.$http.get('http://localhost:8081/api/edituserinfo', { credentials: true, params: { uname: this.u, email: this.e } }).then(response => {
                    console.log(response);
                    window.location.href = "userpage.html"
                }, response => {
                    //erro callbac
                });
            },
            changepassword: function() {
                this.$http.get('http://localhost:8081/api/changepassword', { credentials: true, params: { password: this.p } }).then(response => {
                    console.log(response);
                    window.location.href = "userpage.html"
                }, response => {
                    //erro callbac
                });
            }
        },


        // 对象的初始化方法
        created: function() {
            // GET 
            this.$http.get('http://localhost:8081/api/userinfo', { credentials: true }).then(response => {
                // success callback
                console.log(response);
                var data = response.body.message;
                this.uid = data.uid;
                window.uid = data.uid;
                this.username = data.username;
                window.username = data.username;
                this.email = data.email;
                window.email = data.email;
                this.emailstatus = data.emailstatus;
                window.emailstatus = data.emailstatus;
                this.status = data.status;
                window.status = data.status;
                this.exp = data.exp;
                window.exp = data.exp;
                this.gold = data.gold;
                window.gold = data.gold;
                console.log("window.exp:" + window.exp);
            }, response => {
                // error callback
            });
        }
    })
    // 登录对象
var login = new Vue({
    el: '#login',
    data: {
        email: '',
        password: '',
        passwordRepeat: '',
        showFailAlert: false,
        FailMessage: '登录失败,网络连接失败'
    },
    computed: {
        email_feedback() {
            return this.email.length ? '' : '请输入邮箱';
        },
        email_state() {
            return this.email.length ? 'success' : 'warning';
        },
        pass_feedback() {
            return this.password.length ? '' : '请输入密码';
        },
        pass_state() {
            return this.password.length ? 'success' : 'warning';
        }
    },
    methods: {
        submit: function() {
            // POST /login
            this.$http.get('http://localhost:8081/api/login', { credentials: true, params: { email: this.email, password: this.password } }).then(response => {
                var data = response.body;
                console.log(data.id);
                if (data.id != 0) {
                    this.FailMessage = data.message;
                    this.showFailAlert = true;
                } else {
                    console.log('location');
                    this.showFailAlert = false;
                    window.location.href = "home.html";
                }
            }, response => {
                console.log(response);
                // error callback
                this.showFailAlert = true;
            });
        }
    },
    created: function() {
        // GET 
        this.$http.get('http://localhost:8081/api/login', { credentials: true }).then(response => {
            // success callback
            console.log(response);
            var data = response.body.message;
            this.email = data.email;
            window.email = data.email;
        }, response => {
            // error callback
        });
    }

})

// 注册对象
var login = new Vue({
    el: '#register',
    data: {
        email: '',
        password: '',
        passwordRepeat: '',
        showFailAlert: false,
        FailMessage: '登录失败,网络连接失败'
    },
    computed: {
        email_feedback() {
            return this.email.length ? '' : '请输入邮箱';
        },
        email_state() {
            return this.email.length ? 'success' : 'warning';
        },
        pass_feedback() {
            return this.password.length ? '' : '请输入密码';
        },
        pass_state() {
            return this.password.length ? 'success' : 'warning';
        },
        passr_feedback() {
            return this.password == this.passwordRepeat ? '' : '两次密码不匹配';
        },
        passr_state() {
            return this.password == this.passwordRepeat && this.passwordRepeat.length ? 'success' : 'warning';
        }
    },
    methods: {
        submit: function() {
            // POST /login
            this.$http.get('http://localhost:8081/api/register', { credentials: true, params: { email: this.email, password: this.password } }).then(response => {
                var data = response.body;
                console.log(data);
                if (data.id != 0) {
                    this.FailMessage = data.message;
                    this.showFailAlert = true;
                } else {
                    this.showFailAlert = false;
                    window.location.href = "home.html";
                }
            }, response => {
                console.log(response);
                // error callback
                this.showFailAlert = true;
            });
        }
    }
})

// top10
var top = new Vue({
    el: '#top',
    data: {
        pools: [],
        newpool: '',
        newpooluid: ''
    },
    methods: {
        createPool: function() {
            // GET 
            this.$http.get('http://localhost:8081/pool/save', { credentials: true, params: { 'commonPool.name': this.newpool, 'commonPool.type': 1, 'commonPool.uid': this.newpooluid } }).then(response => {
                // success callback
                console.log(response);
                var data = response.body;
                window.location.reload();
            }, response => {
                // error callback
            });
        }
    },
    // 对象的初始化方法
    created: function() {
        // GET 
        this.$http.get('http://localhost:8081/pool/getall', { credentials: true }).then(response => {
            // success callback
            console.log(response);
            this.pools = response.body;
        }, response => {
            // error callback
        });
    }
})

// 池子
var pool = new Vue({
    el: '#pool',
    data: {
        pools: [],
        newpool: '',
        newpooluid: ''
    },
    methods: {
        createPool: function() {
            // GET 
            this.$http.get('http://localhost:8081/pool/save', { credentials: true, params: { 'commonPool.name': this.newpool, 'commonPool.type': 1, 'commonPool.uid': this.newpooluid } }).then(response => {
                // success callback
                console.log(response);
                var data = response.body;
                window.location.reload();
            }, response => {
                // error callback
            });
        }
    },
    // 对象的初始化方法
    created: function() {
        // GET 
        this.$http.get('http://localhost:8081/pool/getall', { credentials: true }).then(response => {
            // success callback
            console.log(response);
            this.pools = response.body;
        }, response => {
            // error callback
        });
    }
})

//消息
var message = new Vue({
    el: '#message',
    data: {
        text: '',
        poolid: '',
        newtext: '',
        showSuccess: false
    },
    methods: {
        submit() {
            // Ensure modal closes
            this.$http.get('http://localhost:8081/messages/queryOneMessage', { credentials: true, params: { poolid: this.poolid } }).then(response => {
                // success callback
                console.log(response);
                var data = response.body;
                this.text = data.text;
                console.log("shown");
                this.$root.$emit('show::modal', 'modal1');
            }, response => {
                // error callback
            });
        },
        createMsg() {
            this.$http.get('http://localhost:8081/messages/saveMessage', { credentials: true, params: { 'commonMessage.poolid': this.poolid, 'commonMessage.text': this.newtext, 'commonMessage.uid': window.uid } }).then(response => {
                // success callback
                this.showSuccess = true;
                this.newtext = '';
            }, response => {
                // error callback
            });
        }
    },
    created: function() {
        this.poolid = getQueryString('id');

    }
})


// 社交模块
var social = new Vue({
    el: '#social',
    data: {

    },
    methods: {

    },
    // 对象的初始化方法
    created: function() {

    }
})