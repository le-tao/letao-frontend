import Login from './views/Login.vue'
import NotFound from './views/404.vue'
import Home from './views/Home.vue'
import Main from './views/Main.vue'
import Table from './views/nav1/Table.vue'
import Form from './views/nav1/Form.vue'
import ouser from './views/nav1/user.vue'
import Page4 from './views/nav2/Page4.vue'
import Page5 from './views/nav2/Page5.vue'
import Page6 from './views/nav3/Page6.vue'
import echarts from './views/charts/echarts.vue'

import Basic from './views/basic/index.vue'
import User from './views/user/index.vue'
import Check from './views/user/check.vue'
import Group from './views/user/group.vue'
import Pool from './views/pool/index.vue'
import Tags from './views/pool/tags.vue'
import msg from './views/message/index.vue'
import checkmsg from './views/message/check.vue'
import commentmsg from './views/message/comment.vue'

let routes = [
    {
        path: '/login',
        component: Login,
        name: '',
        hidden: true
    },
    {
        path: '/404',
        component: NotFound,
        name: '',
        hidden: true
    },
    //{ path: '/main', component: Main },
    {
        path: '/',
        component: Home,
        name: '',
        leaf: true,//只有一个节点
        iconCls: 'el-icon-setting',//图标样式class
        children: [
            { path: '/', component: Basic, name: '基础信息' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '用户与用户组',
        iconCls: 'fa fa-id-card-o',//图标样式class
        children: [
            { path: '/user', component: User, name: '用户管理'},
            { path: '/check', component: Check, name: '审核用户'},
            { path: '/group', component: Group, name: '用户组管理' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '池子与标签',
        iconCls: 'el-icon-menu',//图标样式class
        children: [
            { path: '/pool', component: Pool, name: '池子管理'},
            { path: '/tags', component: Tags, name: '标签管理' }
        ]
    },
    {
        path: '/',
        component: Home,
        name: '消息管理',
        iconCls: 'fa fa-comments',//图标样式class
        children: [
            { path: '/message', component: msg, name: '消息列表'},
            { path: '/comment', component: commentmsg, name: '评论列表' },
            { path: '/checkmsg', component: checkmsg, name: '消息审核' }
        ]
    },
    {
        path: '*',
        hidden: true,
        redirect: { path: '/404' }
    }
];

export default routes;