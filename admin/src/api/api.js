import axios from 'axios';

const myApi = axios.create({
  baseURL: 'http://localhost:8081',
  withCredentials: true,
  headers: {
    'Accept' : 'application/json',
    'Content-Type': 'application/json',
  }
});

//let baseuri = 'localhost:8081';

//export const getAllUser = params => { return axios.get('http://localhost:8081/admin/user/getall', params).then( res => res ); };
//用户管理
export const getAllUser = function(params){
    return myApi.get('/admin/user/getall',{params:params});
};
export const getAllGroups = function(){
    return myApi.get('/admin/group/findall');
};
export const addUser = params => { return myApi.get(`/admin/user/add`, { params: params }); };
export const editUser = params => { return myApi.get(`/admin/user/modify`, { params: params }); };
export const removeUser = params => { return myApi.get(`/admin/user/del`, { params: params }); };
export const batchRemoveUser = params => { return myApi.get(`/admin/user/batchdel`, { params: params }); };
//登录登出
export const requestLogin = params => { return myApi.get(`/admin/auth/login`, { params: params }).then(res => res.data); };
export const requestLogout = params => { return myApi.get(`/admin/auth/logout`); };
//用户审核
export const getAllChecks = params => { return myApi.get(`/admin/user/getuncheck`); };
export const checkUser = params => { return myApi.get(`/admin/user/check`, { params: params }); };
export const batchCheck = params => { return myApi.get(`/admin/user/batchcheck`, { params: params }); };
//用户组管理
export const delGroup = params => { return myApi.get(`/admin/group/del`, { params: params }); };
export const addGroup = params => { return myApi.get(`/admin/group/add`, { params: params }); };
export const editGroup = params => { return myApi.get(`/admin/group/modify`, { params: params }); };

//标签管理
export const getAllTags = params => { return myApi.get(`/admin/label/findAll`); };
export const delTag = params => { return myApi.get(`/admin/label/delete`, { params: params }); };
export const saveTag = params => { return myApi.get(`/admin/label/save`, { params: params }); };
//池子管理
export const getAllPools = function(params){
    return myApi.get('/admin/pool/getall',{params:params});
};
export const addPool = params => { return myApi.get(`/admin/pool/save`, { params: params }); };
export const editPool = params => { return myApi.get(`/admin/pool/save`, { params: params }); };
export const removePool = params => { return myApi.get(`/admin/pool/delete`, { params: params }); };
export const batchRemovePool = params => { return myApi.get(`/admin/pool/batchdel`, { params: params }); };

//消息管理
export const getMessages = function(params){
    return myApi.get('/admin/msg/getall',{params:params});
};
export const removeMessage = params => { return myApi.get(`/admin/msg/delete`, { params: params }); };
export const batchRemoveMessage = params => { return myApi.get(`/admin/msg/batchdelete`, { params: params }); };
//评论管理
export const getComments = function(params){
    return myApi.get('/admin/comment/getall',{params:params});
};
export const removeComment = params => { return myApi.get(`/admin/comment/delete`, { params: params }); };
export const batchRemoveComment = params => { return myApi.get(`/admin/comment/batchdelete`, { params: params }); };
//举报管理
export const getReports = function(params){
    return myApi.get('/admin/msg/getuncheck',{params:params});
};
export const checkReport = params => { return myApi.get(`/admin/msg/check`, { params: params }); };
export const batchCheckReport = params => { return myApi.get(`/admin/msg/batchcheck`, { params: params }); };

//基础设置
export const getBasicSetting = function(params){
    return myApi.get('/admin/setting/findLatest',{params:params});
};
export const setBasicSetting = params => { return myApi.get(`/admin/setting/save`, { params: params }); };
