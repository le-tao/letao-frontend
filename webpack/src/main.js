import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'


import App from './App.vue'
import ltnav from './ltnav.vue'
import indexcon from './indexcon.vue'
import signin from './signin.vue'
import register from './register.vue'
import userinfo from './userinfo.vue'
import store from './vuex/store'
import msg from './msg.vue'
import userinfo2 from './userinfo2.vue'
import pool from './pool.vue'
import pool2 from './pool2.vue'
import getmsg from './getmsg.vue'
import getmsg2 from './getmsg2.vue'
import createmsg from './createmsg.vue'
import index2 from './index2.vue'
import listpool from './listpool.vue'
import listmsg from './listmsg.vue'


Vue.use(ElementUI)
Vue.use(VueRouter)
Vue.use(Vuex)

const routes = [
    { path: '/', component: indexcon },
    { path: '/index2', component: index2 },
    { path: '/signin', component: signin },
    { path: '/register', component: register },
    { path: '/userinfo', component: userinfo },
    { path: '/userinfo2', component: userinfo2 },
	{ path: '/msg', component: msg },
    { path: '/pool', component: pool },
    { path: '/pool2', component: pool2 },
    { path: '/getmsg', component: getmsg },
    { path: '/getmsg2', component: getmsg2 },
    { path: '/createmsg', component: createmsg },
	{ path: '/listpool', component: listpool },
    { path: '/listmsg', component: listmsg },
	
]

const router = new VueRouter({
    routes
})


const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})