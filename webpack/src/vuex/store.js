import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/login'
import userinfo from './modules/userinfo'
import pool from './modules/pool'
import label from './modules/label'
import msg from './modules/msg'
Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        login: login,
        userinfo: userinfo,
        pool: pool,
        label: label,
        msg: msg,
    },
})


export default store