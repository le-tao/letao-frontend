import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
      count: 0
  },
  actions: {

  },
  mutations: {
      increment: state => state.count++,
      decrement: state => state.count--
  },
  getters: {

  }
})
export default store