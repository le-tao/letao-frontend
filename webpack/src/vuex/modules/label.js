export const LABEL_GET = 'LABEL_GET';

export default {
    state: {
        labels :[],
    },
    actions: {
        getlable({ commit } , labels){
        commit(LABEL_GET , labels);
        },
        
    },
    mutations: {
        [LABEL_GET] (state , labels) {
          state.labels = labels.labels;
        },
    },
    getters: {
        getMsg_labels(state){
            return state.msg;
        }
    }
};