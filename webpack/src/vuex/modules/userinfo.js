export const USER_INFO = 'USER_INFO';
//export const SHOW_LOGIN = 'SHOW_LOGIN';
export default {
    state: {
        email: '',
        uid: '',
        username: '',
        emailstatus: '',
        status: '',
        exp: '',
        gold: ''
    },
    actions: {
        showuserinfo({ commit } , userInfo){
        commit(USER_INFO , userInfo);
        },
        // showLogin({ commit } , flag){
        //   commit(SHOW_LOGIN , flag);
        // },
    },
    mutations: {
        [USER_INFO] (state , userInfo) {
          state.email = userInfo.email;
          state.uid = userInfo.uid;
          state.username = userInfo.username;
          state.emailstatus = userInfo.emailstatus;
          state.status = userInfo.status;
          state.exp = userInfo.exp;
          state.gold = userInfo.gold;
        },

        // [SHOW_LOGIN] (state , flag) {
        //   state.isShowLogin = flag;
        // },
    },
    getters: {
        getMsg_userinfo(state){
            return state.msg;
        }
    }
};