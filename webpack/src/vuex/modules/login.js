export const LOGIN_SUC = 'LOGIN_SUC';
export const SHOW_LOGIN = 'SHOW_LOGIN';
export const LOGOUT_SUC = 'LOGOUT_SUC';
export default {
    state: {
        email: '',
        password: '',
        isShowLogin: false,
    },
    actions: {
        addMyInfo({ commit } , loginInfo){
        commit(LOGIN_SUC , loginInfo);
        },
        showLogin({ commit } , flag){
          commit(SHOW_LOGIN , flag);
        },
        logOut({commit} , flag){
            commit(LOGOUT_SUC , flag);
        }
    },
    mutations: {
        [LOGIN_SUC] (state , loginInfo) {
          state.email = loginInfo.email;
        },

        [SHOW_LOGIN] (state , flag) {
          state.isShowLogin = flag;
        },
        [LOGOUT_SUC] (state , flag){
            state.isShowLogin = flag;
        }
    },
    getters: {
        getMsg_login(state){
            return state.msg;
        }
    }
};