export const POOL_GET = 'POOL_GET';
export const LABEL_SET = 'LABEL_SET';
export const POOL_SET = 'POOL_SET';
export const NEW_SET = 'NEW_SET';
export const NAME_SET = 'NAME_SET';
export default {
    state: {
        pools: [],
        labelid: '0',
        poolid: '0',
        newmsg: '1',
        name:''
    },
    actions: {
        getpool({ commit } , pools){
        commit(POOL_GET , pools);
        },
        setlabel({ commit } , label){
        commit(LABEL_SET , label);
        },
        setpool({ commit } , pool){
        commit(POOL_SET , pool);
        },
        setnew({ commit } , newmsg){
        commit(NEW_SET , newmsg);
    },
        setname({ commit } , name){
        commit(NAME_SET , name);
    },
    },
    mutations: {
        [POOL_GET] (state , pools) {
          state.pools = pools.pools;
        },
        [LABEL_SET] (state , label) {
          state.labelid = label.label;
        },
        [POOL_SET] (state , pool) {
          state.poolid = pool.pool;
        },
        [NEW_SET] (state , newmsg) {
          state.newmsg = newmsg;
        },
        [NAME_SET] (state , name) {
          state.name = name.name;
        },
    },
    getters: {
        getMsg_pool(state){
            return state.msg;
        }
    }
};

