export const MSG_GET = 'MSG_GET';
export const UP = 'UP';
export const UP2 = 'UP2';
export const DOWN = 'DOWN';
export const DOWN2 = 'DOWN2';
export default {
    state: {
        msgid:'',
        msgtext:'',
        msgup:'',
        msgdown:'',
        msgpath:[1],
    },
    actions: {
        getmsg({ commit } , msg){
        commit(MSG_GET , msg);
        },
        up({commit}){
            commit(UP);
        },
        up2({commit}){
            commit(UP2);
        },
        down({commit}){
            commit(DOWN);
        },
        down2({commit}){
            commit(DOWN2);
        },
    },
    mutations: {
        [MSG_GET] (state , msg) {
          state.msgid = msg.id ;
          state.msgtext = msg.text ;
          state.msgup = msg.up ;
          state.msgdown = msg.down ;
          state.msgpath = msg.filepath.split(",");
          for (var i=0;i<state.msgpath.length;i++)
            {
                state.msgpath[i]='http://ooyevurbp.bkt.clouddn.com/'+state.msgpath[i];
            }
        
        },
        [UP] (state ) {
          state.msgup++ ;
        },
        [UP2] (state ) {
          state.msgup++ ;
          state.msgdown--;
        },
        [DOWN] (state ) {
          state.msgdown++;
        },
        [DOWN2] (state ) {
          state.msgup--;
          state.msgdown++;
        },
    },
    getters: {
        getMsg_msg(state){
            return state.msg;
        }
    }
};